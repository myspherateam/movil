### BUILDER
FROM node:13 as BUILDER

RUN npm install -g cordova ionic

WORKDIR /src

COPY package.json .
RUN npm install
COPY . .
RUN ionic build --prod -- --baseHref /mobile --deployUrl /mobile/

### NGINX
FROM nginx:alpine

COPY /docker_conf/nginx.conf /etc/nginx/nginx.conf

WORKDIR /usr/share/nginx/html
COPY --from=BUILDER /src/www/ .
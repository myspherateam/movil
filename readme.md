Preparando el entorno para el desarrollo:

1) Instalar npm
2) Instalar paquetes globales necesarios:
npm i -g @ionic/cli cordova native-run
3) Instalar paquetes de npm:
npm i
4) Preparar ionic y cordova:
ionic cordova prepare

Ejecutando la aplicación en navegador:
npm start
o
ionic serve
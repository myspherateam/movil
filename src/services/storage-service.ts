import { Inject, Injectable } from '@angular/core';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';

@Injectable()
export class LocalStorageService<T> {

    constructor(@Inject(LOCAL_STORAGE) private storage: StorageService) {

    }

    storeOnLocalStorage(key: string, value: T): void {
        this.storage.set(key, value);
    }

    getFromLocalStorage(key: string): T {
        return this.storage.get(key);
    }

    containsKey(key: string) {
        return this.storage.has(key);
    }

    logOut() {
        if (this.storage.has('access_token')) {
            this.storage.remove('access_token');
        }
        if (this.storage.has('refresh_token')) {
            this.storage.remove('refresh_token');
        }
    }

    isSignIn() {
        return this.storage.has('signIn') && this.storage.get('signIn');
    }

    signOut() {
        this.storage.clear();
    }

    remove(key: string) {
        this.storage.remove(key);
    }
}

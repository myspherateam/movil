import { Component, OnDestroy, OnInit } from '@angular/core';

import { Platform, AlertController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { LocalStorageService } from 'src/services/storage-service';
import { ApiService } from './api/api-service';
import { TranslateService } from '@ngx-translate/core';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { Events } from 'src/services/events-service';
import { Constants } from './utils/constants';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  public appPages: any[];
  public allOptions: any[];
  public allParentOptions: any[];
  public simpleOptions: any[];
  public hasOneUser: boolean;
  public isAParent: boolean;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    private localStorage: LocalStorageService<any>,
    private alertController: AlertController,
    private apiService: ApiService,
    public translateService: TranslateService,
    private inAppBrowser: InAppBrowser,
    private events: Events
  ) {
    this.allOptions = [
      {
        title: 'HOME.STAIR',
        url: '/home',
        icon: 'home'
      },
      {
        title: 'HOME_MENU.MY_INSIGNIAS',
        url: '/my-insignias',
        icon: 'ribbon'
      },
      {
        title: 'HOME_MENU.MY_CHALLENGES',
        url: '/my-challenges',
        icon: 'clipboard'
      },
      {
        title: 'HOME_MENU.RANKING',
        url: '/ranking',
        icon: 'podium'
      },
      {
        title: 'HOME_MENU.LOG_OUT',
        icon: 'log-out'
      }, {
        title: 'HOME_MENU.PRIVACY_POLICY',
        icon: 'document-text'
      },
      {
        title: 'HOME_MENU.SETTINGS',
        url: '/settings',
        icon: 'settings'
      }
    ];
    this.allParentOptions = [
      {
        title: 'HOME.STAIR',
        url: '/home',
        icon: 'home'
      },
      {
        title: 'HOME_MENU.MY_INSIGNIAS',
        url: '/my-insignias',
        icon: 'ribbon'
      },
      {
        title: 'HOME_MENU.MY_CHALLENGES',
        url: '/my-challenges',
        icon: 'clipboard'
      },
      {
        title: 'HOME_MENU.LOG_OUT',
        icon: 'log-out'
      }, {
        title: 'HOME_MENU.PRIVACY_POLICY',
        icon: 'document-text'
      },
      {
        title: 'HOME_MENU.SETTINGS',
        url: '/settings',
        icon: 'settings'
      }
    ];
    this.simpleOptions = [
      {
        title: 'HOME.STAIR',
        url: '/home',
        icon: 'home'
      },
      {
        title: 'HOME_MENU.MY_INSIGNIAS',
        url: '/my-insignias',
        icon: 'ribbon'
      },
      {
        title: 'HOME_MENU.MY_CHALLENGES',
        url: '/my-challenges',
        icon: 'clipboard'
      },
      {
        title: 'HOME_MENU.RANKING',
        url: '/ranking',
        icon: 'podium'
      },
      {
        title: 'HOME_MENU.PRIVACY_POLICY',
        icon: 'document-text'
      },
      {
        title: 'HOME_MENU.SETTINGS',
        url: '/settings',
        icon: 'settings'
      }
    ];
    this.initializeApp();
  }

  ngOnInit() {
    // checks the param that displays the diferent menus depending if the user has multiple access
    this.apiService.hasOneUser.subscribe((result) => {
      if (result) {
        this.localStorage.storeOnLocalStorage('hasOneUser', result.hasOneUser);
      }
      this.hasOneUser = this.localStorage.containsKey('hasOneUser') ?
        this.localStorage.getFromLocalStorage('hasOneUser') : false;
    });
    
    this.checkMenu();

    // subscribe to role change event from login page
    this.events.subscribe(Constants.ROLE_CHANGE, () => {
      this.checkMenu();
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleLightContent();
      this.splashScreen.hide();
      this.statusBar.show();
      this.statusBar.styleLightContent();
      console.log('App component. Initialize app method');
    });

    this.platform.backButton.subscribe(it => {
      const currentUrl = this.router.url;

      if (
        currentUrl.includes('/home') ||
        currentUrl.includes('/login') ||
        currentUrl.includes('/register') ||
        currentUrl.includes('/user-selection')
      ) {
        const app = 'app';
        navigator[app].exitApp();
      }
    });
  }
  /**
   * Check if user role has changed to show other menu list
   */
  checkMenu() {
    this.isAParent = this.checkUserRole();

    if (!this.hasOneUser && !this.isAParent) {
      this.appPages = this.allOptions;
    } else if (!this.hasOneUser && this.isAParent) {
      this.appPages = this.allParentOptions;
    } else {
      this.appPages = this.simpleOptions;
    }
  }
  /**
   * Check if user is a parent or a child
   */
  checkUserRole() {
    return !this.localStorage.containsKey('childId');
  }

  onMenuItemClick(page: any) {
    switch (page.title) {
      case 'HOME_MENU.LOG_OUT':
        this.showAlertLogout();
        break;
      case 'HOME_MENU.PRIVACY_POLICY':
        this.inAppBrowser.create('https://test.escalasalut.com/assets/privacy.pdf', '_system', 'location=yes');
        break;
    }
  }

  async showAlertLogout() {
    const alert = await this.alertController.create({
      header: this.translateService.instant('CHANGE_USER_DIALOG.HEADER'),
      message: this.translateService.instant('CHANGE_USER_DIALOG.MESSAGE'),
      buttons: [
        {
          text: this.translateService.instant('VERBS.CANCEL')
        },
        {
          text: this.translateService.instant('VERBS.ACCEPT'),
          handler: () => {
            this.logout();
          }
        }
      ]
    });

    await alert.present();
  }

  logout() {
    if (this.localStorage) {
      this.localStorage.storeOnLocalStorage('signIn', false);
      this.router.navigateByUrl('/login');
    }
  }

  ngOnDestroy(): void {
    this.events.destroy(Constants.ROLE_CHANGE);
  }

}

import { Component, OnInit } from '@angular/core';
import { AlertController, MenuController } from '@ionic/angular';
import { Router } from '@angular/router';
import { FirebaseX } from '@ionic-native/firebase-x/ngx';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-help',
  templateUrl: './help.page.html',
  styleUrls: ['./help.page.scss'],
})
export class HelpPage implements OnInit {

  public question: string;
  public email: string;

  constructor(
    private router: Router,
    public alertController: AlertController,
    private firebase: FirebaseX,
    private menuController: MenuController,
    private translateService: TranslateService
  ) {
    this.menuController.enable(false);
  }

  ngOnInit() {
  }

  ionViewDidEnter() {
    const screenName = 'help';
    this.firebase.logEvent('select_content', { content_type: 'page_view', item_name: screenName });
    this.firebase.setScreenName(screenName);
  }

  sendHelp() {
    this.presentAlert();
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: this.translateService.instant('HELP.DIALOG_HEADER'),
      subHeader: this.translateService.instant('HELP.DIALOG_SUBHEADER'),
      message: this.translateService.instant('HELP.DIALOG_MESSAGE'),
      buttons: [{
        text: this.translateService.instant('VERBS.ACCEPT'),
        handler: () => {
          this.router.navigateByUrl('home');
        }
      }]
    });

    alert.present();
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EnterStudentDataPage } from './enter-student-data.page';

describe('EnterStudentDataPage', () => {
  let component: EnterStudentDataPage;
  let fixture: ComponentFixture<EnterStudentDataPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnterStudentDataPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EnterStudentDataPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

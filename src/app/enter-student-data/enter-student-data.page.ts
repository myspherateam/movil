import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { FirebaseX } from '@ionic-native/firebase-x/ngx';

@Component({
  selector: 'app-enter-student-data',
  templateUrl: './enter-student-data.page.html',
  styleUrls: ['./enter-student-data.page.scss'],
})
export class EnterStudentDataPage implements OnInit {

  public birthDate: string;
  public selectedSex: string;
  public maxDatetime: string;

  constructor(
    private router: Router,
    private firebase: FirebaseX,
    private menuController: MenuController
  ) {
    this.menuController.enable(false);
    this.maxDatetime = new Date().toISOString();
  }

  ngOnInit() {
  }

  ionViewDidEnter() {
    const screenName = 'enter-student-data';
    this.firebase.logEvent('select_content', { content_type: 'page_view', item_name: screenName });
    this.firebase.setScreenName(screenName);
  }

  onDataAdded() {
    console.log(this.selectedSex, this.birthDate);
    this.selectedSex = '';
    this.birthDate = '';
    this.router.navigateByUrl('enter-parent-data');
  }

}

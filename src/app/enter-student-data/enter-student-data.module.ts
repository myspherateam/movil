import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EnterStudentDataPageRoutingModule } from './enter-student-data-routing.module';

import { EnterStudentDataPage } from './enter-student-data.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EnterStudentDataPageRoutingModule
  ],
  declarations: [EnterStudentDataPage]
})
export class EnterStudentDataPageModule {}

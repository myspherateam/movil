import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EnterStudentDataPage } from './enter-student-data.page';

const routes: Routes = [
  {
    path: '',
    component: EnterStudentDataPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EnterStudentDataPageRoutingModule {}

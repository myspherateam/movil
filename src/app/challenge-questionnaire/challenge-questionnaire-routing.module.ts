import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChallengeQuestionnairePage } from './challenge-questionnaire.page';

const routes: Routes = [
  {
    path: '',
    component: ChallengeQuestionnairePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChallengeQuestionnairePageRoutingModule {}

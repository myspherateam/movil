import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import * as Survey from 'survey-angular';
import theme from './theme.json';

// We are creating the theme object 'paido' here.
Survey.StylesManager.ThemeColors.paido = theme;
Survey.StylesManager.applyTheme('paido');

@Component({
  selector: 'survey',
  templateUrl: './questionnaire.component.html',
  styleUrls: ['./questionnaire.component.scss']
})
export class QuestionnaireComponent implements OnInit {
  @Output() result: EventEmitter<any> = new EventEmitter<any>();
  @Input() json: any;
  @Input() surveyMode: string;

  constructor() { }

  ngOnInit() {
    const surveyModel = new Survey.Model(this.json);

    surveyModel.onComplete.add((result, options) => {
      this.result.emit(result.data);
    });

    // 'edit' for editable survey. 'display' for read-only survey
    surveyModel.mode = this.surveyMode;
    surveyModel.locale = 'es';
    const defaultColor = 'default';
    const defaultThemeStyles = Survey.StylesManager.ThemeColors[defaultColor];
    defaultThemeStyles['$body-container-background-color'] = '#ffffff';
    defaultThemeStyles['$text-color'] = '#000';
    Survey.StylesManager.applyTheme();
    Survey.SurveyNG.render('surveyElement', { model: surveyModel });
  }
}

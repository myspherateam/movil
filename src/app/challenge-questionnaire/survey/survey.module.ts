import { QuestionnaireComponent } from './questionnaire/questionnaire.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [QuestionnaireComponent],
  imports: [
    CommonModule
  ],
  exports: [QuestionnaireComponent]
})
export class SurveyModule { }

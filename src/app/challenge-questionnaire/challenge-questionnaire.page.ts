import { Component, OnInit } from '@angular/core';
import { ChallengeCompletion } from '../models/challenge-completion.js';
import { Router } from '@angular/router';
import { ApiService } from '../api/api-service.js';
import { LocalStorageService } from 'src/services/storage-service';
import { ApiResponse } from '../models/api-response.js';
import { AlertController, AnimationController, NavController } from '@ionic/angular';
import { HttpErrorResponse } from '@angular/common/http';
import { Constants } from '../utils/constants.js';
import { Events } from 'src/services/events-service';
import { FirebaseX } from '@ionic-native/firebase-x/ngx';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-challenge-questionnaire',
  templateUrl: './challenge-questionnaire.page.html',
  styleUrls: ['./challenge-questionnaire.page.scss'],
})
export class ChallengeQuestionnairePage implements OnInit {

  public questionnaireJson: any;
  public challenge: ChallengeCompletion;
  public challengeCompleted: boolean;

  constructor(
    public router: Router,
    public apiService: ApiService,
    public localStorage: LocalStorageService<any>,
    public alertController: AlertController,
    public navController: NavController,
    private events: Events,
    private firebase: FirebaseX,
    private translateService: TranslateService,
    private animationController: AnimationController
  ) {
    if (this.router.getCurrentNavigation().extras && this.router.getCurrentNavigation().extras.state) {
      this.challenge = this.router.getCurrentNavigation().extras.state.challenge;
      if (this.challenge.questionnaire) {
        this.questionnaireJson = JSON.parse(this.challenge.questionnaire);
      }
    }
  }

  ngOnInit() {
  }

  ionViewDidEnter() {
    const screenName = 'challenge-questionnaire';
    this.firebase.logEvent('select_content', { content_type: 'page_view', item_name: screenName });
    this.firebase.setScreenName(screenName);
  }

  onQuestionnaireResult(event: any) {
    this.challengeCompleted = true;

    if (this.challenge.news) {
      this.challenge.newsAnswer = JSON.stringify(event);
    } else {
      this.challenge.questionnaireAnswer = JSON.stringify(event);
    }
  }

  markChallengeAsCompleted(event: CustomEvent) {
    const finishDatetime = new Date(this.challenge.periodFinishDatetime).getTime();
    const startDatetime = new Date(this.challenge.periodStartDatetime).getTime();
    const actualDatetime = new Date().getTime();
    console.log('tiempo de finalización: ' + finishDatetime + ' tiempo actual: ' + actualDatetime);

    if (actualDatetime >= startDatetime && actualDatetime < finishDatetime) {
      let childId: string;
      if (this.localStorage.containsKey('childId')) {
        childId = this.localStorage.getFromLocalStorage('childId');
      }

      const lastAwardedPoints = this.challenge.awardedPoints;

      this.apiService.markAChallengeAsComplete(this.challenge, childId).subscribe(
        (result: ApiResponse<ChallengeCompletion>) => {
          if (result.status === 200) {
            const challengeCompletion: ChallengeCompletion = result.message;
            const pointsSaved = challengeCompletion.awardedPoints - lastAwardedPoints;

            this.showAlertDialog(
              this.translateService.instant('CHALLENGE_DETAIL.DONE'),
              this.translateService.instant('CHALLENGE_DETAIL.CHALLENGE_DONE') +
              pointsSaved +
              this.translateService.instant('CHALLENGE_DETAIL.POINTS'),
              this.translateService.instant('CHALLENGE_DETAIL.CHALLENGE_OVERCOME'),
              'completion-modal',
              true
            );
          }
        },
        (error: HttpErrorResponse) => {
          console.log(error);
          if (error.error) {
            const apiError: ApiResponse<string> = error.error;
            if (apiError.message) {
              this.showAlertDialog(
                this.translateService.instant('ERRORS.TRY_AGAIN'),
                this.translateService.instant('ERRORS.CHALLENGE_FAILED_TITLE'),
                this.translateService.instant('ERRORS.CHALLENGE_FAILED'),
                undefined,
                false
              );
            }
          }
        }
      );
    }
  }

  async showAlertDialog(header: string, subHeader: string, message: string, cssClass: string, animated: boolean) {
    const alert = await this.alertController.create({
      header,
      subHeader,
      message,
      buttons: [{
        text: this.translateService.instant('VERBS.ACCEPT'),
        handler: () => {
          // challenge was completed. Go back to challenges list
          if (!header.includes('Error')) {
            this.events.publish(Constants.CHALLENGE_COMPLETED, {});
            this.navController.pop();
          }
          setTimeout(() => {
            this.navController.pop();
          }, 0);
        }
      }],
      cssClass,
      animated,
      enterAnimation: (baseEl: any, opts?: any) => {
        return (this.animationController
          .create()
          .addElement(baseEl.querySelector('.alert-wrapper'))
          .duration(1000)
          .keyframes([
            { offset: 0, transform: 'scale(1)', opacity: '1' },
            { offset: 0.5, transform: 'scale(1.2)', opacity: '0.3' },
            { offset: 1, transform: 'scale(1)', opacity: '1' }
          ]));
      }
    });
    await alert.present();
  }

}

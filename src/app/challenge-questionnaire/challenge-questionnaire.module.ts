import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ChallengeQuestionnairePageRoutingModule } from './challenge-questionnaire-routing.module';

import { ChallengeQuestionnairePage } from './challenge-questionnaire.page';
import { SurveyModule } from './survey/survey.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChallengeQuestionnairePageRoutingModule,
    SurveyModule,
    TranslateModule
  ],
  declarations: [ChallengeQuestionnairePage]
})
export class ChallengeQuestionnairePageModule {}

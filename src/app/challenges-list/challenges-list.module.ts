import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ChallengesListPageRoutingModule } from './challenges-list-routing.module';

import { ChallengesListPage } from './challenges-list.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChallengesListPageRoutingModule,
    TranslateModule
  ],
  declarations: [ChallengesListPage]
})
export class ChallengesListPageModule {}

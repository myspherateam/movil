import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ChallengesListPage } from './challenges-list.page';

describe('ChallengesListPage', () => {
  let component: ChallengesListPage;
  let fixture: ComponentFixture<ChallengesListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChallengesListPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ChallengesListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

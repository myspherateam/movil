import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { IonSelect, MenuController } from '@ionic/angular';
import { NavigationExtras, ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../api/api-service';
import { ApiResponse } from '../models/api-response';
import { ChallengeCompletion } from '../models/challenge-completion';
import { LocalStorageService } from 'src/services/storage-service';
import { Constants } from '../utils/constants';
import { Events } from 'src/services/events-service';
import { FirebaseX } from '@ionic-native/firebase-x/ngx';
import { TranslateService } from '@ngx-translate/core';
import { ChallengeType } from '../models/challenge-type.model';

@Component({
  selector: 'app-challenges-list',
  templateUrl: './challenges-list.page.html',
  styleUrls: ['./challenges-list.page.scss'],
})
export class ChallengesListPage implements OnInit, OnDestroy {

  private readonly CHALLENGE = 'challenges';
  private readonly ACTIVITIES = 'activities';
  private readonly NEWS = 'news';
  private readonly QUESTIONNAIRE = 'questionnaires';

  public challenges: Array<ChallengeCompletion>;
  public allChallenges: Array<ChallengeCompletion>;
  public challengeSubcategory: string;
  public challengeCategory: string;
  public toolbarTitle: string;
  public isLoading: boolean;
  public type = 'all';
  public finished = false;
  public completed = false;
  @ViewChild('challengeTypeFilter', { static: false }) challengeTypeSelect: IonSelect;
  public isParent: boolean;

  constructor(
    public router: Router,
    private challengeService: ApiService,
    private route: ActivatedRoute,
    private menuController: MenuController,
    private localStorage: LocalStorageService<any>,
    private firebase: FirebaseX,
    private events: Events,
    private translateService: TranslateService
  ) {
    this.menuController.enable(false);
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.challengeSubcategory = this.router.getCurrentNavigation().extras.state.subcategory;
        this.challengeCategory = this.router.getCurrentNavigation().extras.state.step;
      }
    });
    this.isParent = this.localStorage.getFromLocalStorage('childId') === undefined;
  }

  ngOnInit() {
    this.setContentData();
    this.events.subscribe(Constants.CHALLENGE_COMPLETED, () => {
      this.getChallenges();
      this.events.publish(Constants.COUNT_CHANGED, {});
    });
  }

  ionViewDidEnter() {
    const screenName = 'challenges-list';
    this.firebase.logEvent('select_content', { content_type: 'page_view', item_name: screenName });
    this.firebase.setScreenName(screenName);
  }

  ngOnDestroy(): void {
    this.events.destroy(Constants.CHALLENGE_COMPLETED);
  }

  setContentData() {
    this.isLoading = true;
    switch (this.challengeSubcategory) {
      case this.CHALLENGE:
        this.toolbarTitle = this.translateService.instant('STEP_DETAIL.CHALLENGES');
        break;
      case this.ACTIVITIES:
        this.toolbarTitle = this.translateService.instant('STEP_DETAIL.ACTIVITIES');
        break;
      case this.NEWS:
        this.toolbarTitle = this.translateService.instant('STEP_DETAIL.NEWS');
        break;
      case this.QUESTIONNAIRE:
        this.toolbarTitle = this.translateService.instant('STEP_DETAIL.QUESTIONNAIRES');
        break;
    }
    this.getChallenges();
  }

  getChallenges(type: string = null) {
    let childId: string;
    if (this.localStorage.containsKey('childId')) {
      childId = this.localStorage.getFromLocalStorage('childId');
    }
    this.challengeService.getCompletionChallenges(
      this.challengeCategory,
      this.challengeSubcategory,
      type,
      this.finished,
      childId,
      this.completed
    ).subscribe(
      (response: ApiResponse<ChallengeCompletion[]>) => {
        if (response.status === 200) {
          this.allChallenges = response.message;
          this.challenges = response.message;
          this.isLoading = false;
        }
      },
      error => console.log('Error challenges: ' + error)
    );
  }

  onChallengeClick(challenge: ChallengeCompletion) {
    // go to challenge detail
    const navigationExtras: NavigationExtras = {
      state: {
        challenge: challenge.id
      }
    };

    this.router.navigateByUrl('/challenge-detail', navigationExtras);
  }
  /**
   * Check if challenge start date is today
   */
  isStartDateToday(challenge: ChallengeCompletion): boolean {
    const challengeStartDate: Date = new Date(challenge.periodStartDatetime);
    const today: Date = new Date();
    return challengeStartDate.setHours(0, 0, 0, 0) === today.setHours(0, 0, 0, 0);
  }

  /**
   * FILTER METHODS
   */

  onSearch(ev: CustomEvent) {
    this.isLoading = true;
    // filter list
    const value = ev.detail.value;

    if (value) {
      if (value.trim !== '') {
        this.challenges = this.allChallenges.filter(term => {
          return term.title.toLowerCase().indexOf(value.trim().toLowerCase()) > -1;
        });
      }
      this.isLoading = false;
    } else {
      this.challenges = this.allChallenges;
      this.isLoading = false;
    }
  }

  filterList(ev: CustomEvent) {
    // get filter list selected
    this.completed = false;
    this.finished = false;
    const listType = ev.detail.value;
    this.finished = listType === 'past' ? true : false;
    this.completed = listType === 'completed' ? true : false;

    // reload data with new filters
    this.getChallenges(this.type);
  }

  onFiltersClick() {
    this.challengeTypeSelect.open();
  }

  filterByType(ev: CustomEvent) {
    // filter by challenge type
    this.type = ev.detail.value;
    if (this.type === 'all') { this.type = null; }
    this.getChallenges(this.type);
  }

  challengeIsOnTime(challenge: ChallengeCompletion): boolean {
    const finishDatetime = this.getFormattedDateInMillis(
      challenge.periodFinishDatetime
    );
    const startDatetime = this.getFormattedDateInMillis(
      challenge.periodStartDatetime
    );
    const actualDatetime = new Date().getTime();
    return actualDatetime >= startDatetime && actualDatetime < finishDatetime;
  }

  getFormattedDateInMillis(dateString: string): number {
    return new Date(dateString).getTime();
  }

  checkHaveToAuthorize(challenge: ChallengeCompletion): boolean {
    // check if challenge is authority type, user is the parent and challenge has started
    return challenge.type === ChallengeType.authority &&
      this.isParent &&
      this.challengeIsOnTime(challenge);
  }
}

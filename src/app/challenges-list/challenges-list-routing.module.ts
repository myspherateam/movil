import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChallengesListPage } from './challenges-list.page';

const routes: Routes = [
  {
    path: '',
    component: ChallengesListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChallengesListPageRoutingModule {}

import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./register/register.module').then(m => m.RegisterPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then(m => m.LoginPageModule),
    runGuardsAndResolvers: 'always'
  },
  {
    path: 'user-selection',
    loadChildren: () => import('./user-selection/user-selection.module').then(m => m.UserSelectionPageModule)
  },
  {
    path: 'enter-student-data',
    loadChildren: () => import('./enter-student-data/enter-student-data.module').then(m => m.EnterStudentDataPageModule)
  },
  {
    path: 'enter-parent-data',
    loadChildren: () => import('./enter-parent-data/enter-parent-data.module').then(m => m.EnterParentDataPageModule)
  },
  {
    path: 'step-detail',
    loadChildren: () => import('./step-detail/step-detail.module').then(m => m.StepDetailPageModule)
  },
  {
    path: 'challenge-news',
    loadChildren: () => import('./challenge-news/challenge-news.module').then(m => m.ChallengeNewsPageModule)
  },
  {
    path: 'my-insignias',
    loadChildren: () => import('./my-insignias/my-insignias.module').then(m => m.MyInsigniasPageModule)
  },
  {
    path: 'settings',
    loadChildren: () => import('./settings/settings.module').then(m => m.SettingsPageModule)
  },
  {
    path: 'help',
    loadChildren: () => import('./help/help.module').then(m => m.HelpPageModule)
  },
  {
    path: 'profile',
    loadChildren: () => import('./profile/profile.module').then(m => m.ProfilePageModule)
  },
  {
    path: 'challenges-list',
    loadChildren: () => import('./challenges-list/challenges-list.module').then(m => m.ChallengesListPageModule)
  },
  {
    path: 'challenge-detail',
    loadChildren: () => import('./challenge-detail/challenge-detail.module').then(m => m.ChallengeDetailPageModule)
  },
  {
    path: 'challenge-questionnaire',
    loadChildren: () => import('./challenge-questionnaire/challenge-questionnaire.module').then(m => m.ChallengeQuestionnairePageModule)
  },
  {
    path: 'my-challenges',
    loadChildren: () => import('./my-challenges/my-challenges.module').then(m => m.MyChallengesPageModule)
  },
  {
    path: 'about',
    loadChildren: () => import('./about/about.module').then(m => m.AboutPageModule)
  },  {
    path: 'ranking',
    loadChildren: () => import('./ranking/ranking.module').then( m => m.RankingPageModule)
  }




];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      preloadingStrategy: PreloadAllModules,
      onSameUrlNavigation: 'reload'
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { FirebaseX } from '@ionic-native/firebase-x/ngx';
import { Market } from '@ionic-native/market/ngx';
import { AlertController, MenuController, Platform } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Events } from 'src/services/events-service';
import { LocalStorageService } from 'src/services/storage-service';
import { ApiService } from '../api/api-service';
import { ApiResponse } from '../models/api-response';
import { Login } from '../models/login';
import { User } from '../models/user';
import { Constants } from '../utils/constants';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  public users: User[];
  public role: string;

  constructor(
    private router: Router,
    private apiService: ApiService,
    private localStorage: LocalStorageService<any>,
    private menuController: MenuController,
    private alertController: AlertController,
    private appVersion: AppVersion,
    private platform: Platform,
    private firebase: FirebaseX,
    private market: Market,
    private translateService: TranslateService,
    private events: Events
  ) { }
  /**
   * OnInit implementation. Subscribe refresh token event to reload login
   */
  ngOnInit(): void {
    this.events.subscribe(Constants.REFRESH_TOKEN,
      () => {
        this.initComponent();
      }
    );
  }
  /**
   * Init component data
   */
  ionViewWillEnter() {
    this.initComponent();
  }
  /**
   * Launch firebase log event
   */
  ionViewDidEnter() {
    const screenName = 'login';
    this.firebase.logEvent('select_content', { content_type: 'page_view', item_name: screenName });
    this.firebase.setScreenName(screenName);
  }
  /**
   * Triggers initial methods
   */
  initComponent() {
    this.menuController.enable(false);
    this.checkVersion();
  }
  /**
   * Check current app version
   */
  checkVersion() {
    this.platform.ready().then(() => {
      this.appVersion.getVersionNumber().then((value: string) => {

        const currentPlatform: string = this.platform.platforms().includes('android') ? 'android' : 'ios';
        const currentVersion = value;

        this.apiService.checkCurrentVersion(currentPlatform, currentVersion).subscribe(
          (result: ApiResponse<number>) => {
            console.log(result.message);
            switch (result.message) {
              case 1:
                this.login();
                break;
              case 0:
                this.showVersionAlert(
                  false,
                  this.translateService.instant('VERSION.UPDATE_TITLE'),
                  this.translateService.instant('VERSION.UPDATE_DESC')
                );
                break;
              case -1:
                this.showVersionAlert(
                  true,
                  this.translateService.instant('VERSION.UPDATE_TITLE'),
                  this.translateService.instant('VERSION.UPDATE_DESC_NOW')
                );
                break;
            }
          });

      });
    });
  }
  /**
   * Show a dialog to alert user to update version
   * @param needUpdate false to update later, true to update mandatorily
   * @param title dialog title
   * @param subtitle dialog subtitle
   */
  async showVersionAlert(needUpdate: boolean, title: string, subtitle: string) {
    let buttons: any[];
    if (needUpdate) {
      buttons = [
        {
          text: this.translateService.instant('VERBS.ACCEPT'),
          handler: () => {
            // go to market
            this.market.open('com.mysphera.paido');
          }
        }
      ];
    } else {
      buttons = [
        {
          text: this.translateService.instant('VERSION.LATER')
        },
        {
          text: this.translateService.instant('VERBS.ACCEPT'),
          handler: () => {
            // go to market
            this.market.open('com.mysphera.paido');
          }
        }
      ];
    }

    const alert = await this.alertController.create({
      header: title,
      message: subtitle,
      backdropDismiss: false,
      buttons
    });

    await alert.present();
  }
  /**
   * Check if user is logged
   */
  login() {
    const accessToken = this.localStorage.getFromLocalStorage('access_token');
    const username = this.localStorage.getFromLocalStorage('username');
    const password = this.localStorage.getFromLocalStorage('password');
    if (!accessToken) {
      // system does not have a token, but it has user and pass. This is token expired case.
      if (username && password) {
        this.apiService.login(username, password).subscribe(
          (result: Login) => {
            console.log('Login result: ' + result.access_token);
            this.localStorage.storeOnLocalStorage('access_token', result.access_token);
            this.localStorage.storeOnLocalStorage('refresh_token', result.refresh_token);

            this.registerCurrentVersion();
          },
          (response: HttpErrorResponse) => {
            console.log(response);
            if (response.error.error_description) {
              if (response.error.error_description.includes('Invalid user credentials')) {
                this.showAlertError();
              }
            }
          }
        );
      } else {
        // system does not have token, user and pass. Redirect to register
        console.log('redirect register');
        this.router.navigateByUrl('/register');
      }
    } else {
      this.getMe();
    }
  }
  /**
   * Register current app version on server
   */
  registerCurrentVersion() {
    this.platform.ready().then(() => {
      this.appVersion.getVersionNumber().then((value: string) => {

        const currentPlatform: string = this.platform.platforms().includes('android') ? 'android' : 'ios';
        const currentVersion = value;

        this.apiService.registerCurrentVersion(currentPlatform, currentVersion).subscribe(
          (result: ApiResponse<User>) => {
            this.getMe();
          });
      });
    });
  }
  /**
   * Get my user instance
   */
  getMe() {
    this.apiService.getMeMobile().subscribe(
      (result: ApiResponse<User[]>) => {
        if (result.status === 200) {
          this.users = result.message;
          const count = result.count;
          this.saveUserAndRedirect(count);
        }
      },
      error => console.log(error)
    );
  }
  /**
   * Save user instance and redirect to next page. 
   * This method is triggered when user opens de app from background or when user comes from register page
   * @param count total roles inside the user.
   * Count with value 1 indicates that user enters with a child role.
   */
  saveUserAndRedirect(count: number) {
    // CHILD
    if (count === 1) {
      // save my user. I'm a child
      const child = this.users[0];
      this.localStorage.storeOnLocalStorage('user', child);
      this.apiService.hasOneUser.next({ hasOneUser: true });

      // save user points
      this.localStorage.storeOnLocalStorage('userPoints', child.points ? child.points : 0);

      // check if user comes from register page
      if (!child.displayName) {
        const navigationExtras: NavigationExtras = {
          state: {
            user: child
          }
        };
        this.router.navigateByUrl('/user-selection', navigationExtras);
      } else {
        this.router.navigateByUrl('/home');
      }

    } else {
      // PARENT
      // save my user. I'm parent or a parent that enter as a child
      let userParent: User;
      this.users.forEach((user: User) => {
        // check if parent entered as a child, if not save the parent user
        if (this.localStorage.containsKey('childId')) {
          //parent entered as a child
          const childId = this.localStorage.getFromLocalStorage('childId');
          if (user.id === childId) {
            this.localStorage.storeOnLocalStorage('user', user);
            // save user points
            this.localStorage.storeOnLocalStorage('userPoints', user.points ? user.points : 0);
          }
        } else if (user.roles.includes('Parent')) {
          // i'm a parent
          userParent = user;
          this.localStorage.storeOnLocalStorage('user', userParent);
          // save user points
          this.localStorage.storeOnLocalStorage('userPoints', userParent.points ? userParent.points : 0);
        }
      });

      this.apiService.hasOneUser.next({ hasOneUser: false });
      const isAlreadyRegistered = this.localStorage.getFromLocalStorage('isAlreadyRegistered');
      const isSignIn: boolean = this.localStorage.isSignIn();

      // check if the parent has been already registered or not. If not, keep in login page.
      if (!isAlreadyRegistered && userParent) {
        const navigationExtras: NavigationExtras = {
          state: {
            user: userParent
          }
        };
        this.router.navigateByUrl('/user-selection', navigationExtras);
      } else if (isAlreadyRegistered && isSignIn) {
        this.router.navigateByUrl('/home');
      }
    }
    this.events.publish(Constants.ROLE_CHANGE, {});
  }
  /**
   * Alert that there is a session active in other device
   */
  async showAlertError() {
    const alert = await this.alertController.create({
      header: this.translateService.instant('ERRORS.ERROR'),
      message: this.translateService.instant('ERRORS.USER_SESSION_ACTIVE'),
      buttons: [
        {
          text: this.translateService.instant('VERBS.ACCEPT'),
          handler: () => {
            this.router.navigateByUrl('/register');
          }
        }
      ],
      backdropDismiss: false
    });

    await alert.present();
  }
  /**
   * Get the user role
   * @param roles user roles
   */
  getRole(roles: string[]) {
    if (roles.includes('Parent')) {
      return this.translateService.instant('LOGIN.ROLE_PARENT');
    } else if (roles.includes('Child')) {
      return this.translateService.instant('LOGIN.ROLE_CHILD');
    }
  }
  /**
   * User click on a user button in login page
   * @param user user instance
   */
  goHome(user: User) {
    this.localStorage.storeOnLocalStorage('signIn', true);
    console.log(user);
    // user is a parent that enter like a child
    if (this.users.length > 1 && user.roles.includes('Child')) {
      // save childId
      this.localStorage.storeOnLocalStorage('childId', user.id);
      this.localStorage.storeOnLocalStorage('childDisplayId', user.displayName ? user.displayName : user.displayId);
    } else {
      // user is a child or a parent
      if (this.localStorage.containsKey('childId')) {
        this.localStorage.remove('childId');
        this.localStorage.remove('childDisplayId');
      }
    }

    // save user points
    this.localStorage.storeOnLocalStorage('userPoints', user.points ? user.points : 0);
    this.localStorage.storeOnLocalStorage('user', user);

    if (this.localStorage.containsKey('access_token')) {
      // notify app page that user role has changed and needs to refresh menu list
      this.events.publish(Constants.ROLE_CHANGE, {});
      this.router.navigateByUrl('/home');
    }
  }
  /**
   * Unsubscribe refresh token event
   */
  ionViewWillLeave() {
    this.events.destroy(Constants.REFRESH_TOKEN);
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { StepDetailPage } from './step-detail.page';

describe('StepDetailPage', () => {
  let component: StepDetailPage;
  let fixture: ComponentFixture<StepDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepDetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(StepDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

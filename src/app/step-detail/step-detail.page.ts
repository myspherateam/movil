import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { SubcategoriesCount } from '../models/subcategories-count';
import { Constants } from '../utils/constants';
import { ApiResponse } from '../models/api-response';
import { PendingCount } from '../models/pending-count';
import { ApiService } from '../api/api-service';
import { Events } from 'src/services/events-service';
import { FirebaseX } from '@ionic-native/firebase-x/ngx';
import { LocalStorageService } from 'src/services/storage-service';

@Component({
  selector: 'app-step-detail',
  templateUrl: './step-detail.page.html',
  styleUrls: ['./step-detail.page.scss'],
})
export class StepDetailPage implements OnInit, OnDestroy {

  public title: string;
  public key: string;
  public count: SubcategoriesCount;
  public keyPrepare = 'prepare';
  public keyActivate = 'activate';
  public keyAvoid = 'avoid';
  public keyEnjoy = 'enjoy';
  public keyKeep = 'keep';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private menuController: MenuController,
    private apiService: ApiService,
    private firebase: FirebaseX,
    private events: Events,
    private localStorage: LocalStorageService<any>
  ) {
    this.menuController.enable(false);
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.title = this.router.getCurrentNavigation().extras.state.title;
        this.key = this.router.getCurrentNavigation().extras.state.key;
      }
    });
  }

  ngOnInit() {
    this.getCount();
    this.events.subscribe(Constants.COUNT_CHANGED, () => {
      this.getCount();
    });
  }

  ionViewDidEnter() {
    const screenName = 'step-detail';
    this.firebase.logEvent('select_content', { content_type: 'page_view', item_name: screenName });
    this.firebase.setScreenName(screenName);
  }

  ngOnDestroy(): void {
    this.events.destroy(Constants.COUNT_CHANGED);
  }

  async getCount() {
    let childId: string = undefined;
    if (this.localStorage.containsKey('childId')) {
      childId = this.localStorage.getFromLocalStorage('childId');
    }
    const apiResponse: ApiResponse<PendingCount> = await this.apiService.getPendingChallengeProgramming(childId).toPromise();
    if (apiResponse.status === 200) {
      const pendingCount: PendingCount = apiResponse.message;
      switch (this.key) {
        case this.keyPrepare:
          this.count = pendingCount.prepare.subcategories;
          break;
        case this.keyActivate:
          this.count = pendingCount.activate.subcategories;
          break;
        case this.keyAvoid:
          this.count = pendingCount.avoid.subcategories;
          break;
        case this.keyEnjoy:
          this.count = pendingCount.enjoy.subcategories;
          break;
        case this.keyKeep:
          this.count = pendingCount.keep.subcategories;
          break;
      }
    }
  }

  openChallengesList(subcategory: string) {
    const navigationExtras: NavigationExtras = {
      state: {
        subcategory,
        step: this.key
      }
    };
    this.router.navigateByUrl('/challenges-list', navigationExtras);
  }

}

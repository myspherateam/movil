import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { FirebaseX } from '@ionic-native/firebase-x/ngx';
import { Router } from '@angular/router';
import { User } from '../models/user';
import { LocalStorageService } from 'src/services/storage-service';
import { AnimalMap } from '../models/animal-map';
import { ColorMap } from '../models/color-map';
import { ApiService } from '../api/api-service';
import { ApiResponse } from '../models/api-response';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  public user: User;
  public localUser: User;
  public displayName: string;
  public displayNumber: string;
  public points: number;
  public animalName: string;
  public animalColor: string;
  public isAChild: boolean = false;

  constructor(
    private menuController: MenuController,
    private firebase: FirebaseX,
    private router: Router,
    private localStorage: LocalStorageService<any>,
    private apiService: ApiService
  ) {
    this.menuController.enable(false);
    if (this.router.getCurrentNavigation().extras.state) {
      this.localUser = this.router.getCurrentNavigation().extras.state.user;
    }
  }

  ngOnInit() {
    this.apiService.getMeMobile().subscribe(
      (result: ApiResponse<User[]>) => {
        if (result.status === 200) {
          const users = result.message;
          users.forEach((element: User) => {
            if (element.id === this.localUser.id) {
              this.user = element;
            }
          });
          console.log(this.user);

          if (this.user) {
            if (this.localStorage.containsKey('childDisplayId')) {
              // user is a parent that enter like a child
              this.getDisplayName(String(this.localStorage.getFromLocalStorage('childDisplayId')));
              this.isAChild = true;
            } else {
              // user is parent or unique child
              this.getDisplayName(this.user.displayName);

              if (this.user.roles.includes('Child')) {
                this.isAChild = true;
              } else {
                this.isAChild = false;
              }
            }
            if (this.localStorage.containsKey('userPoints')) {
              this.points = this.localStorage.getFromLocalStorage('userPoints');
            }
          }

        }
      },
      error => console.log(error)
    );
  }

  ionViewDidEnter() {
    const screenName = 'profile';
    this.firebase.logEvent('select_content', { content_type: 'page_view', item_name: screenName });
    this.firebase.setScreenName(screenName);
  }

  getDisplayName(displayName: string) {
    this.displayName = '';
    this.displayNumber = '';

    if (displayName) {
      // user have displayName
      for (let i = 0; i < displayName.length; i++) {
        const char = displayName.charAt(i);
        if (char >= '0' && char <= '9') {
          this.displayNumber += char;
        } else {
          this.displayName += char;
        }
      }
      if (this.displayName)
        this.getUserIcon(this.displayName);
    } else {
      // user is parent
      this.displayName = String(this.user.displayId);
    }
  }
  /**
   * Get animal and color of user icon to show it
   * @param displayName user displayName
   */
  getUserIcon(displayName: string) {
    const splitName = displayName.replace(/([a-z](?=[A-Z]))/g, '$1 ');
    this.animalName = AnimalMap.get(splitName.split(' ')[0].toString());
    this.animalColor = ColorMap.get(splitName.split(' ')[1].toString());
  }
  /**
   * Get total number of challenges assigned
   */
  getNumberAssignedChallenges(): number {
    const numAssignedChallenges = this.user.numAssignedChallenges;
    if (numAssignedChallenges) {
      return numAssignedChallenges.authority +
        numAssignedChallenges.gps +
        numAssignedChallenges.news +
        numAssignedChallenges.physical +
        numAssignedChallenges.questionnaire;
    }
    return 0;
  }
  /**
   * Get total number of challenges completed
   */
  getNumberCompletedChallenges(): number {
    const numCompletedChallenges = this.user.numCompletedChallenges;
    if (numCompletedChallenges) {
      return numCompletedChallenges.authority +
        numCompletedChallenges.gps +
        numCompletedChallenges.news +
        numCompletedChallenges.physical +
        numCompletedChallenges.questionnaire;
    }
    return 0;
  }
  /**
   * Get number of challenges not completed
   */
  getNumberOfNotCompletedChallenges(): number {
    return this.getNumberAssignedChallenges() - this.getNumberCompletedChallenges();
  }

}

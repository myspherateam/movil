import { Component, OnInit } from '@angular/core';
import { FirebaseX } from '@ionic-native/firebase-x/ngx';
import { AppVersion } from '@ionic-native/app-version/ngx';

@Component({
  selector: 'app-about',
  templateUrl: './about.page.html',
  styleUrls: ['./about.page.scss'],
})
export class AboutPage implements OnInit {

  public versionNumber: string;

  constructor(
    private firebase: FirebaseX,
    private appVersion: AppVersion
  ) { }

  ngOnInit() {
    this.appVersion.getVersionNumber().then(
      (value: string) => {
        this.versionNumber = value;
      }
    );
  }

  ionViewDidEnter() {
    const screenName = 'about';
    this.firebase.logEvent('select_content', { content_type: 'page_view', item_name: screenName });
    this.firebase.setScreenName(screenName);
  }

}

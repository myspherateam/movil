import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UserSelectionPage } from './user-selection.page';

describe('UserSelectionPage', () => {
  let component: UserSelectionPage;
  let fixture: ComponentFixture<UserSelectionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserSelectionPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UserSelectionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

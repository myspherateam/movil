import { Component, OnInit } from '@angular/core';
import { PickerController, MenuController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../api/api-service';
import { ApiResponse } from '../models/api-response';
import { User } from '../models/user';
import { ColorMap } from '../models/color-map';
import { AnimalMap } from '../models/animal-map';
import { LocalStorageService } from 'src/services/storage-service';
import { FirebaseX } from '@ionic-native/firebase-x/ngx';
import { TranslateService } from '@ngx-translate/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-user-selection',
  templateUrl: './user-selection.page.html',
  styleUrls: ['./user-selection.page.scss'],
})

export class UserSelectionPage implements OnInit {

  public name: string;
  public nameColumns = [];
  public user: User;
  public animalColor: string;
  public animalName: string;
  public passwordGroup: FormGroup;
  public passwordControl: FormControl;
  public registerPassed: boolean;

  constructor(
    public pickerController: PickerController,
    private router: Router,
    private menuController: MenuController,
    private apiService: ApiService,
    private route: ActivatedRoute,
    private firebase: FirebaseX,
    private localStorage: LocalStorageService<any>,
    private translateService: TranslateService
  ) {
    this.menuController.enable(false);
    this.name = '';
    this.passwordGroup = new FormGroup(
      {
        passwordControl: new FormControl(
          null,
          Validators.compose(
            [
              Validators.required,
              Validators.minLength(8)
            ]
          )
        )
      }
    );
    this.route.queryParams.subscribe((params) => {
      if (this.router.getCurrentNavigation().extras) {
        if (this.router.getCurrentNavigation().extras.state) {
          this.user = this.router.getCurrentNavigation().extras.state.user;
        }
      }
    });
  }

  ngOnInit() {
    this.getNameColumns();
  }

  ionViewDidEnter() {
    const screenName = 'user-selection';
    this.firebase.logEvent('select_content', { content_type: 'page_view', item_name: screenName });
    this.firebase.setScreenName(screenName);
  }

  getNameColumns() {
    const animals = [];
    const colors = [];

    AnimalMap.forEach((value: string, key: string) => {
      animals.push(key);
    });
    ColorMap.forEach((value: string, key: string) => {
      colors.push(key);
    });

    this.nameColumns = [animals, colors];
  }

  async openPicker() {
    const picker = await this.pickerController.create({
      buttons: [
        {
          text: this.translateService.instant('VERBS.CANCEL'),
          role: 'cancel'
        },
        {
          text: this.translateService.instant('VERBS.CONFIRM'),
          handler: (value) => {
            const keys: string[] = Object.keys(value);
            let selection = '';

            if (keys.length > 0) {
              this.animalName = AnimalMap.get(value[keys[0]].text);
              this.animalColor = ColorMap.get(value[keys[1]].text);
              console.log(this.animalName + ' ' + this.animalColor);
              keys.forEach((key: string) => {
                const text = value[key].text;
                selection += text;
              });
            }

            this.name = selection;
            this.checkRegisterData();
          }
        }
      ],
      columns: this.getColumns()
    });

    picker.present();
  }
  /**
   * Check if all required data is valid
   */
  checkRegisterData() {
    if (this.user.roles.includes('Parent')) {
      this.registerPassed = this.passwordGroup.valid;
    } else {
      this.registerPassed = this.name && this.passwordGroup.valid;
    }
  }

  getColumns() {
    const columns = [];

    for (let i = 0; i < this.nameColumns.length; i++) {
      columns.push({
        name: 'column-' + i,
        options: this.getColumnOptions(i, this.nameColumns[i].length)
      });
    }

    return columns;
  }

  getColumnOptions(index: number, length: number) {
    const options = [];

    for (let i = 0; i < length; i++) {
      options.push({
        text: this.nameColumns[index][i],
        value: i
      });
    }

    return options;
  }

  onSelectionDone() {
    const password: string = this.passwordGroup.get('passwordControl').value;
    this.apiService.checkAvailableDisplayName(this.name).subscribe(
      (result: ApiResponse<boolean>) => {
        if (result.message === true) {
          this.user.displayName = this.name;
          this.user.password = password;
          this.apiService.createUser(this.user).subscribe(
            (userResult: ApiResponse<User>) => {
              if (userResult.status === 200) {
                const user = userResult.message;
                this.localStorage.storeOnLocalStorage('user', user);
                this.localStorage.storeOnLocalStorage('password', password);
                this.localStorage.storeOnLocalStorage('isAlreadyRegistered', true);

                if (user.roles.includes('Parent')) {
                  this.apiService.hasOneUser.next({ hasOneUser: false });
                  this.router.navigateByUrl('/login');
                } else {
                  this.apiService.hasOneUser.next({ hasOneUser: true });
                  this.router.navigateByUrl('/home');
                }
              }
            });
        }
      });
  }

}

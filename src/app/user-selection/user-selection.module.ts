import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UserSelectionPageRoutingModule } from './user-selection-routing.module';

import { UserSelectionPage } from './user-selection.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UserSelectionPageRoutingModule,
    TranslateModule,
    ReactiveFormsModule
  ],
  declarations: [UserSelectionPage]
})
export class UserSelectionPageModule {}

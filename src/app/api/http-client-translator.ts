import { HttpClient, HttpBackend } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class HttpClientTranslator extends HttpClient {
  constructor(handler: HttpBackend) {
    super(handler);
  }
}

export function HttpLoaderFactory(httpClient: HttpClientTranslator) {
  return new TranslateHttpLoader(httpClient, './assets/i18n/', '.json');
}

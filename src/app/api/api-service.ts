import { Injectable } from '@angular/core';
import { ChallengeProgramming } from '../models/challenge-programming';
import { User } from '../models/user';
import { Observable } from 'rxjs/internal/Observable';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { ApiResponse } from '../models/api-response';
import { ApiConfig } from './api-config';
import { ChallengeCompletion } from '../models/challenge-completion';
import { LocalStorageService } from 'src/services/storage-service';
import { Login } from '../models/login';
import { Login as Token } from '../models/login';
import { PendingCount } from '../models/pending-count';
import { Register } from '../models/register';
import { BehaviorSubject } from 'rxjs';
import { Badge } from '../models/badge';
import { UserBadges } from '../models/user-badges';
import { Ranking } from '../models/ranking';

@Injectable({
    providedIn: 'root'
})
export class ApiService {

    public accessToken: string;
    public hasOneUser = new BehaviorSubject(null);

    constructor(
        private http: HttpClient,
        private localStorage: LocalStorageService<any>
    ) { }

    // API CALLS

    register(registrationCode: string): Observable<ApiResponse<Register>> {
        const params = new HttpParams().set('registrationCode', registrationCode);
        const options = { params };
        return this.http.get<ApiResponse<Register>>(ApiConfig.baseUrl + '/administration/users/credentials', options);
    }

    login(username: string, password: string): Observable<Login> {
        const httpParams = new HttpParams()
            .set('username', username)
            .set('password', password)
            .set('grant_type', 'password')
            .set('client_id', 'frontend');
        return this.http.post<Login>(
            ApiConfig.baseUrl + '/auth/realms/paido/protocol/openid-connect/token',
            httpParams
        );
    }

    refreshToken(): Observable<Token> {
        const refreshToken = this.localStorage.getFromLocalStorage('refresh_token');
        const httpParams = new HttpParams()
            .set('grant_type', 'refresh_token')
            .set('client_id', 'frontend')
            .set('refresh_token', refreshToken);
        return this.http.post<Token>(
            ApiConfig.baseUrl + '/auth/realms/paido/protocol/openid-connect/token',
            httpParams
        );
    }

    getAuthHeader(): HttpHeaders {
        this.accessToken = this.localStorage.getFromLocalStorage('access_token');
        return new HttpHeaders({ Authorization: 'Bearer ' + this.accessToken });
    }

    // ADMINISTRATION
    getMeMobile(): Observable<ApiResponse<User[]>> {
        return this.http.get<ApiResponse<User[]>>(ApiConfig.baseUrl + '/administration/me/mobile');
    }

    checkAvailableDisplayName(displayName: string): Observable<ApiResponse<boolean>> {
        const httpParams = new HttpParams()
            .set('displayName', displayName);
        return this.http.get<ApiResponse<boolean>>(ApiConfig.baseUrl + '/administration/users/available', { params: httpParams });
    }

    createUser(body: User): Observable<ApiResponse<User>> {
        return this.http.post<ApiResponse<User>>(ApiConfig.baseUrl + '/administration/users/mobile', body);
    }

    checkCurrentVersion(platform: string, version: string): Observable<ApiResponse<number>> {
        const queryParams = new HttpParams()
            .set('platform', platform)
            .set('currentVersion', version);
        console.log(platform + ' ' + version);
        return this.http.get<ApiResponse<number>>(
            ApiConfig.baseUrl + '/administration/version/check',
            { params: queryParams }
        );
    }

    registerCurrentVersion(platform: string, version: string): Observable<ApiResponse<User>> {
        const body = {
            platform,
            currentVersion: version
        };
        return this.http.post<ApiResponse<User>>(ApiConfig.baseUrl + '/administration/version/register', body);
    }
    /**
     * Get user ranking
     */
    getRanking(childId: string, field: string, rankingType: string): Observable<ApiResponse<Ranking>> {
        let queryParams = new HttpParams();
        if (childId) { queryParams = queryParams.set('childId', childId); }
        if (field) { queryParams = queryParams.set('field', field); }
        if (rankingType) { queryParams = queryParams.set('rankingType', rankingType); }
        return this.http.get<ApiResponse<Ranking>>(`${ApiConfig.baseUrl}/administration/me/ranking`, { params: queryParams });
    }

    // CHALLENGES
    /*
        filters:
        category(prepare, activate, avoid, enjoy, keep)
        subcategory(challenges, news, questionnaires, activities)
        type(physical, authority, gps, questionnaire, news)
        finished(true, false)
    */
    getProgrammingChallenges(
        category: string,
        subcategory: string,
        type: string,
        finished: boolean
    ): Observable<ApiResponse<ChallengeProgramming[]>> {
        let queryParams = new HttpParams()
            .set('finished', String(finished));
        if (category) { queryParams = queryParams.set('category', category); }
        if (subcategory) { queryParams = queryParams.set('subcategory', subcategory); }
        if (type) { queryParams = queryParams.set('type', type); }

        const options = {
            params: queryParams
        };

        return this.http.get<ApiResponse<ChallengeProgramming[]>>(ApiConfig.baseUrl + '/challenges/challengeProgramming', options);
    }

    getProgrammingChallengeDetail(programmingId: string): Observable<ApiResponse<ChallengeProgramming>> {
        return this.http.get<ApiResponse<ChallengeProgramming>>(ApiConfig.baseUrl + '/challenges/challengeProgramming/' + programmingId);
    }

    markAChallengeAsComplete(body: ChallengeCompletion, childId: string): Observable<ApiResponse<ChallengeCompletion>> {
        let queryParams = new HttpParams();
        if (childId) { queryParams = queryParams.set('childId', childId); }
        return this.http.post<ApiResponse<ChallengeCompletion>>(
            ApiConfig.baseUrl + '/challenges/challengeCompletion', body, { params: queryParams }
        );
    }

    getPendingChallengeProgramming(childId: string): Observable<ApiResponse<PendingCount>> {
        let queryParams = new HttpParams();
        if (childId) { queryParams = queryParams.set('childId', childId); }
        return this.http.get<ApiResponse<PendingCount>>(
            ApiConfig.baseUrl + '/challenges/challengeCompletion/count', { params: queryParams }
        );
    }

    getCompletionChallenges(
        category: string,
        subcategory: string,
        type: string,
        finished: boolean,
        childId: string,
        completed?: boolean
    ): Observable<ApiResponse<ChallengeCompletion[]>> {
        let queryParams = new HttpParams()
            .set('finished', String(finished));
        if (category) { queryParams = queryParams.set('category', category); }
        if (subcategory) { queryParams = queryParams.set('subcategory', subcategory); }
        if (type && type !== 'all') { queryParams = queryParams.set('type', type); }
        if (childId) { queryParams = queryParams.set('childId', childId); }
        if (completed) { queryParams = queryParams.set('completed', String(completed)); }

        const options = {
            params: queryParams
        };

        return this.http.get<ApiResponse<ChallengeCompletion[]>>(ApiConfig.baseUrl + '/challenges/challengeCompletion', options);
    }

    getCompletionChallengeDetail(completionId: string, childId: string): Observable<ApiResponse<ChallengeCompletion>> {
        let queryParams = new HttpParams();
        if (childId) { queryParams = queryParams.set('childId', childId); }
        return this.http.get<ApiResponse<ChallengeCompletion>>(
            ApiConfig.baseUrl + '/challenges/challengeCompletion/' + completionId, { params: queryParams }
        );
    }
    /**
     * Get next physical challenge to accomplish
     */
    getNextPhysicalCompletion(childId: string): Observable<ApiResponse<ChallengeCompletion[]>> {
        let queryParams = new HttpParams();
        if (childId) { queryParams = queryParams.set('childId', childId); }
        return this.http.get<ApiResponse<ChallengeCompletion[]>>(
            ApiConfig.baseUrl + '/challenges/challengeCompletion/nextPhysical', { params: queryParams }
        );
    }
    /**
     * Get user badges
     */
    getUserBadges(userId: string, childId: string): Observable<ApiResponse<UserBadges>> {
        let queryParams = new HttpParams()
        if (childId) queryParams = queryParams.set('childId', childId);
        return this.http.get<ApiResponse<UserBadges>>(ApiConfig.baseUrl + `/challenges/badge/forId/${userId}`, { params: queryParams });
    }

    getNextBadge(type: string, childId: string): Observable<ApiResponse<Badge>> {
        let queryParams = new HttpParams().set('type', type);
        if (childId) { queryParams = queryParams.set('childId', childId); }
        return this.http.get<ApiResponse<Badge>>(ApiConfig.baseUrl + `/challenges/badge/nextBadge`, { params: queryParams });
    }
    /**
     * Get relatives pending challenges
     */
    getRelativesPendingChallenges(childId: string): Observable<ApiResponse<ChallengeCompletion[]>> {
        let queryParams = new HttpParams().set('inverted', 'true');
        queryParams = queryParams.set('started', 'true');
        if (childId) { queryParams = queryParams.set('childId', childId); }
        return this.http.get<ApiResponse<ChallengeCompletion[]>>(`${ApiConfig.baseUrl}/challenges/challengeCompletion`, { params: queryParams });
    }
}

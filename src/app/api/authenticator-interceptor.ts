import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Observable, from, NEVER } from 'rxjs';
import { ApiService } from './api-service';
import { LocalStorageService } from 'src/services/storage-service';
import { flatMap, mergeMap } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router } from '@angular/router';
import { Events } from 'src/services/events-service';
import { Constants } from '../utils/constants';

@Injectable({
    providedIn: 'root'
})
export class AuthenticatorInterceptor implements HttpInterceptor {

    constructor(
        public apiService: ApiService,
        public storageService: LocalStorageService<any>,
        public jwtHelper: JwtHelperService,
        public router: Router,
        public events: Events
    ) {
        this.jwtHelper = new JwtHelperService();
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const token: string = this.storageService.getFromLocalStorage('access_token');
        const tokenrf: string = this.storageService.getFromLocalStorage('refresh_token');

        console.log('Intercept url request ' + request.url);
        console.log('token expire date: ' + this.jwtHelper.getTokenExpirationDate(token));
        console.log('token refresh expire date: ' + this.jwtHelper.getTokenExpirationDate(tokenrf));

        const promise: Promise<Observable<HttpEvent<any>>> = new Promise(async (resolve) => {
            if (!request.url.includes('token')) {
                if (token && this.jwtHelper.isTokenExpired(token)) {
                    // if token expires
                    console.log('token is expired, proceed refresh');
                    const result = await this.apiService.refreshToken().toPromise()
                        .catch((error: HttpErrorResponse) => {
                            console.log(error);
                            console.log('Proceed login');
                            this.storageService.logOut();
                            this.router.navigateByUrl('/login');
                            this.events.publish(Constants.REFRESH_TOKEN, {});
                            resolve(NEVER);
                        });

                    if (result) {
                        this.storageService.storeOnLocalStorage('access_token', result.access_token);
                        this.storageService.storeOnLocalStorage('refresh_token', result.refresh_token);

                        request = request.clone({
                            setHeaders: { Authorization: `Bearer ${result.access_token}` }
                        });
                    } else {
                        // reset token and send to login
                        this.storageService.logOut();
                        this.router.navigateByUrl('/login');
                        resolve(NEVER);
                    }

                } else if (token) {
                    // if token is available
                    request = request.clone({
                        setHeaders: { Authorization: `Bearer ${token}` }
                    });
                }

            }
            resolve(next.handle(request));
        });

        return from(promise)
            .pipe(
                mergeMap(it => it)
            );
    }

}

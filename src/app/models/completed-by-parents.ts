export class CompletedByParents {
    public parentId: string;
    public completionDatetime?: string;
}

export class LocationPoint {
    public type: 'Point';
    public coordinates: [number, number];
}

export class LocationPolygon {
    public type: 'Polygon';
    public coordinates: [number, number][][];
}

export type Location = LocationPoint | LocationPolygon;

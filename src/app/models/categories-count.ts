import { SubcategoriesCount } from './subcategories-count';

export class CategoriesCount {
    public count: number;
    public subcategories: SubcategoriesCount;
}

export enum ChallengeType {
    physical = 'physical',
    authority = 'authority',
    gps = 'gps',
    questionnaire = 'questionnaire',
    news = 'news'
}

export class Badge {
    public id: string;
    public name?: string;
    public description?: string;
    public image?: any;
    public code?: string;
    public currentProgress?: number;
    public nextMilestone?: number;
}
import { TopUser } from "./top-user";

export class Ranking {
    public myPosition: number;
    public myValue: number;
    public totalUsers: number;
    public topUsers: TopUser[];
}
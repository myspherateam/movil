import { Point } from './point';
import { Location } from './location';

export class ChallengeDefinition {

    public id: string;
    public title: string;
    public description: string;
    public explanation: string;
    public preWarn: boolean;
    public periodDays: number;
    public periodAmount: number;
    public category: string;
    public subcategory: string;
    public points: Point;
    /**
     * AssignedFor can have this values: parent | child
     */
    public assignedFor: string[];
    public type: string;
    public minAge: number;
    public maxAge: number;

    // Type: Physical
    public steps: number;

    // Type: Authority
    public childRelation: string[];
    public specificIds: number[];

    // Type: GPS
    public location: Location;
    public radiusMeters: number;

    // Type: Questionnaire
    public questionnaire: string;

    // Type: News
    public news: string;

    constructor() {
    }

}

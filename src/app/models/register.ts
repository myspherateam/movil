export class Register {
    public username: string;
    public password: string;
    public roles: string[];
    public isAlreadyRegistered: boolean;
}

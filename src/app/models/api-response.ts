export class ApiResponse<T> {
    public message: T;
    public status: number;
    public statusMessage: string;
    public count: number;
    public errorCode: string;
}

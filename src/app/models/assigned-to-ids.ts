export class AssignedToIds {
    public users: string[];
    public groups: string[];
    public entities: string[];
}

import { Badge } from "./badge";

export class UserBadges {
    badges: Badge[];
    futureBadges: Badge[];
}

import { NumberAssignedChallenges } from "./number-assigned-challenges";
import { NumberCompletedChallenges } from "./number-completed-challenges";

export class User {

    public id: string;
    public roles: string[];
    public displayId: number;
    public entityIds: string[];
    public groupIds: string[];
    public caringForIds: string[];
    public creator: string;
    public displayName: string;
    public password: string;
    public points: number;
    public steps: number;
    public numAssignedChallenges: NumberAssignedChallenges;
    public numCompletedChallenges: NumberCompletedChallenges;

    constructor() { }

}

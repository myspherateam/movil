export const ColorMap: Map<string, string> = new Map<string, string>([
    ['Negro', 'rgb(0,0,0)'],
    ['Morado', 'rgb(98, 43, 173)'],
    ['Menta', 'rgb(114, 237, 183)'],
    ['Azul', 'rgb(62, 121, 230)'],
    ['Rojo', 'rgb(227, 30, 30)'],
    ['Amarillo', 'rgb(250, 187, 12)'],
    ['Naranja', 'rgb(235, 124, 21)'],
    ['Verde', 'rgb(14, 181, 17)'],
    ['Magenta', 'rgb(255, 0, 255)'],
    ['Cian', 'rgb(7, 235, 235)'],
    ['Granate', 'rgb(176, 54, 72)'],
    ['Rosa', 'rgb(235, 162, 206)']
]);

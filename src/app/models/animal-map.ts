export const AnimalMap: Map<string, string> = new Map<string, string>([
    ['Mamba', 'mamba'],
    ['Unicornio', 'unicornio'],
    ['Zorro', 'zorro'],
    ['Tiburon', 'tiburon'],
    ['Perro', 'perro'],
    ['Gato', 'gato'],
    ['Leon', 'leon'],
    ['Pinguino', 'pinguino'],
    ['Aguila', 'aguila'],
    ['Gallo', 'gallo'],
    ['Ardilla', 'ardilla'],
    ['Mariposa', 'mariposa']
]);

import { CategoriesCount } from './categories-count';

export class PendingCount {
    public prepare: CategoriesCount;
    public activate: CategoriesCount;
    public avoid: CategoriesCount;
    public enjoy: CategoriesCount;
    public keep: CategoriesCount;
}

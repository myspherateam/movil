import { ChallengeDefinition } from './challenge-definition';
import { AssignedToIds } from './assigned-to-ids';

export class ChallengeProgramming extends ChallengeDefinition {

    public id: string;
    public definitionId: string;
    public programedById: string;
    public startDatetime: string;
    public finishDatetime: string;
    public visibleDatetime: string;
    public overridesIds: string[];
    public assignedToIds: AssignedToIds[];

    constructor() {
        super();
    }

}

import { Location } from './location';
import { CompletedByParents } from './completed-by-parents';
import { ChallengeProgramming } from './challenge-programming';

export class ChallengeCompletion extends ChallengeProgramming {

    public id: string;
    public definitionId: string;
    public programmingId: string;
    public forId: string;
    public displayName: string;
    public completionDatetime: string;
    public completed: boolean;
    public completedByParents: CompletedByParents[];
    public periodStartDatetime: string;
    public periodFinishDatetime: string;
    public important: boolean;
    public active: boolean;
    public awardedPoints: number;

    // Type: Physical
    public userSteps: number;

    // Type: Authority
    public approvedById: string;

    // Type: GPS
    public userLocation: Location;
    public precision: number;

    // Type: Questionnaire
    public questionnaireAnswer: string;

    // Type: News
    public newsAnswer: string;

    constructor() {
        super();
    }

}

export class SubcategoriesCount {
    public challenges: number;
    public activities: number;
    public news: number;
    public questionnaires: number;
}

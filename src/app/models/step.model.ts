import { CategoriesCount } from './categories-count';

export class Step {
    constructor(
        public index,
        public name: string,
        public key: string,
        public width: number,
        public pendingCount: CategoriesCount
    ) { }
}

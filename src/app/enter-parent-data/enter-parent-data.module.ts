import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EnterParentDataPageRoutingModule } from './enter-parent-data-routing.module';

import { EnterParentDataPage } from './enter-parent-data.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EnterParentDataPageRoutingModule
  ],
  declarations: [EnterParentDataPage]
})
export class EnterParentDataPageModule {}

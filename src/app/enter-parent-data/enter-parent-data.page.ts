import { Component, OnInit } from '@angular/core';
import { AlertController, MenuController } from '@ionic/angular';
import { Router } from '@angular/router';
import { LocalStorageService } from 'src/services/storage-service';
import { FirebaseX } from '@ionic-native/firebase-x/ngx';

@Component({
  selector: 'app-enter-parent-data',
  templateUrl: './enter-parent-data.page.html',
  styleUrls: ['./enter-parent-data.page.scss'],
})
export class EnterParentDataPage implements OnInit {

  public birthDate: string;
  public selectedSex: string;
  public maxDatetime: string;

  constructor(
    private router: Router,
    public alertController: AlertController,
    private menuController: MenuController,
    private firebase: FirebaseX,
    private localStorage: LocalStorageService<any>
  ) {
    this.menuController.enable(false);
    this.maxDatetime = new Date().toISOString();
    console.log(this.maxDatetime);
  }

  ngOnInit() {
  }

  ionViewDidEnter() {
    const screenName = 'enter-parent-data';
    this.firebase.logEvent('select_content', { content_type: 'page_view', item_name: screenName });
    this.firebase.setScreenName(screenName);
  }

  onDataAdded() {
    console.log(this.selectedSex, this.birthDate);
    this.selectedSex = '';
    this.birthDate = '';
    // show register confirmed
    this.presentAlert();
  }

  async presentAlert() {
    this.localStorage.storeOnLocalStorage('signIn', true);
    const alert = await this.alertController.create({
      header: 'Creado',
      subHeader: 'Registro realizado correctamente',
      message: 'El usuario ha sido creado. Pulse Acceder para entrar en la aplicación.',
      buttons: [{
        text: 'Acceder',
        handler: () => {
          this.router.navigateByUrl('/login');
        }
      }]
    });

    await alert.present();
  }
}

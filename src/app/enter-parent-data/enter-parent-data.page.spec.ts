import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EnterParentDataPage } from './enter-parent-data.page';

describe('EnterParentDataPage', () => {
  let component: EnterParentDataPage;
  let fixture: ComponentFixture<EnterParentDataPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnterParentDataPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EnterParentDataPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';

import { HomePage } from './home.page';
import { NgCircleProgressModule } from 'ng-circle-progress';
import { TranslateModule } from '@ngx-translate/core';
import { PipesModule } from '../pipes/pipes-module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePage
      }
    ]),
    NgCircleProgressModule.forRoot({
      radius: 70,
      space: -8,
      outerStrokeWidth: 8,
      innerStrokeWidth: 8,
      animation: false,
      showUnits: false,
      showTitle: false,
      showSubtitle: false,
      imageSrc: '../../assets/icono-paido.PNG',
      imageWidth: 80,
      imageHeight: 90,
      showImage: true,
      showBackground: false,
      maxPercent: 100,
      responsive: true
    }),
    TranslateModule,
    PipesModule
  ],
  declarations: [HomePage]
})
export class HomePageModule { }

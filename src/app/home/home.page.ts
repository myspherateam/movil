import { Component } from '@angular/core';
import { Step } from '../models/step.model';
import { NavigationExtras, Router } from '@angular/router';
import { StepNames } from '../models/step-names';
import { AlertController, MenuController, Platform } from '@ionic/angular';
import { ApiService } from '../api/api-service';
import { PendingCount } from '../models/pending-count';
import { ApiResponse } from '../models/api-response';
import { CategoriesCount } from '../models/categories-count';
import { LocalStorageService } from 'src/services/storage-service';
import { User } from '../models/user';
import { FirebaseX } from '@ionic-native/firebase-x/ngx';
import { Health } from '@ionic-native/health/ngx';
import { ChallengeCompletion } from '../models/challenge-completion';
import { TranslateService } from '@ngx-translate/core';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { Badge } from '../models/badge';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  public stepsText: Map<string, string>;
  public steps = new Array<Step>();
  private pendingCountStep: CategoriesCount[] = [];
  public user: User;
  public displayName: string;
  public displayNumber: string;
  // color variables for circle progress component. White color by default.
  public outerStrokeColor = '#ffffff';
  public innerStrokeColor = '#ffffff';
  public progressPercent = 0;
  private childId: string;
  public pointProgress = 0;
  public userPoints: number;
  public showBadgeProgress: boolean = false;
  public nextBadgeId: string;
  public havePendingChallenges: boolean = false;
  public relativesPendingCount: number;

  constructor(
    public router: Router,
    private menuController: MenuController,
    private apiService: ApiService,
    private firebase: FirebaseX,
    private localStorage: LocalStorageService<any>,
    private health: Health,
    private platform: Platform,
    private translateService: TranslateService,
    private alertController: AlertController,
    private androidPermissions: AndroidPermissions
  ) {
    this.stepsText = new StepNames(this.translateService).names;
  }

  ionViewDidEnter() {
    const screenName = 'home';
    this.firebase.logEvent('select_content', { content_type: 'page_view', item_name: screenName });
    this.firebase.setScreenName(screenName);
  }

  ionViewWillEnter() {
    if (this.localStorage.containsKey('user')) {
      this.user = this.localStorage.getFromLocalStorage('user');
      if (this.localStorage.containsKey('childDisplayId')) {
        // user is a parent that enter like a child
        this.getDisplayName(String(this.localStorage.getFromLocalStorage('childDisplayId')));
      } else {
        // user is parent or unique child
        this.getDisplayName(this.user.displayName);
      }
    }
    if (this.localStorage.containsKey('childId')) {
      this.childId = this.localStorage.getFromLocalStorage('childId');
    } else {
      this.childId = undefined;
    }
    this.menuController.enable(true);
    this.getUserPoints();
    this.generateSteps();
    this.getUserSteps();
    this.getRelativesPendingChallenges();
  }

  getDisplayName(displayName: string) {
    this.displayName = '';
    this.displayNumber = '';

    if (displayName) {
      // user have displayName
      for (let i = 0; i < displayName.length; i++) {
        const char = displayName.charAt(i);
        if (char >= '0' && char <= '9') {
          this.displayNumber += char;
        } else {
          this.displayName += char;
        }
      }
    } else {
      // user is parent
      this.displayName = String(this.user.displayId);
    }
  }

  getUserPoints() {
    console.log('childId: ' + this.childId + ' roles: ' + this.user.roles);
    if (this.childId || this.user.roles.includes('Child')) {
      this.showBadgeProgress = true;
      this.apiService.getNextBadge('points', this.childId).subscribe(
        (result: ApiResponse<Badge>) => {
          if (result.status == 200) {
            const badge = result.message;
            const currentProgress = badge.currentProgress ? badge.currentProgress : 0;
            const nextMilestone = badge.nextMilestone;
            this.nextBadgeId = badge.id;
            console.log('current: ' + currentProgress + ' next: ' + nextMilestone);
            this.userPoints = currentProgress;
            if (currentProgress > 0 && nextMilestone > 0) {
              this.pointProgress = currentProgress / nextMilestone;
            }
          }
        }
      );
    } else {
      this.showBadgeProgress = false;
    }
  }

  async generateSteps() {
    this.steps = new Array<Step>();
    let width = (this.platform.is('ipad') || this.platform.is('tablet')) ? 75 : 100;
    let i = 0;

    // get pending count home steps
    const apiResponse: ApiResponse<PendingCount> = await this.apiService.getPendingChallengeProgramming(this.childId).toPromise();
    if (apiResponse.status === 200) {
      const pendingCount: PendingCount = apiResponse.message;
      this.pendingCountStep = [
        pendingCount.prepare,
        pendingCount.activate,
        pendingCount.avoid,
        pendingCount.enjoy,
        pendingCount.keep
      ];
    }
    this.stepsText.forEach((value: string, key: string) => {
      const step: Step = {
        index: i,
        name: value,
        key,
        width,
        pendingCount: this.pendingCountStep[i]
      };
      this.steps.push(step);
      width -= (this.platform.is('ipad') || this.platform.is('tablet')) ? 12 : 15;
      i++;
    });

    this.steps.reverse();
  }

  onStepClick(step: Step) {
    const navigationExtras: NavigationExtras = {
      state: {
        title: step.name,
        key: step.key
      }
    };
    this.router.navigateByUrl('/step-detail', navigationExtras);
  }

  openHelp() {
    this.router.navigateByUrl('/help');
  }

  goToProfile() {
    const navigationExtras: NavigationExtras = {
      state: {
        user: this.user
      }
    };
    this.router.navigateByUrl('/profile', navigationExtras);
  }
  /**
   * Get user steps and total steps from next challenge to accomplish
   */
  getUserSteps() {
    this.getPhysicalSteps();
  }
  /**
   * Check if cordova platform and health platform are ready to request data
   */
  async getPhysicalSteps() {
    // check platform ready
    const ready = !!(await this.platform.ready());
    if (ready && this.platform.is('cordova')) {
      // check plugin is ready
      this.checkHealthPlugin();
    }
  }
  /**
   * Check if health platform is ready to request permissions
   */
  checkHealthPlugin() {
    this.health
      .isAvailable()
      .then((available: boolean) => {
        // request activity recognition permission for android 10
        this.androidPermissions.requestPermission('android.permission.ACTIVITY_RECOGNITION')
          .then(result => {
            console.log('Activity recognition permission: ' + result.hasPermission);
            // Authorize using the requestAuthorization method in health plugin
            this.requestHealthPermissions();
          })
          .catch(console.error);
      })
      .catch((error) => console.log(error));
  }
  /**
   * Request health permissions to read step data
   */
  requestHealthPermissions() {
    this.health
      .requestAuthorization([{ read: ['steps'] }])
      .then((result) => {
        console.log('Authorization request: ' + result);
        this.getNextPhysicalChallenge();
      })
      .catch((error) => {
        console.log('Authorization request: ' + error);
      });
  }
  /**
   * Get total steps from next physical challenge
   */
  getNextPhysicalChallenge() {
    this.apiService.getNextPhysicalCompletion(this.childId).subscribe(
      (response: ApiResponse<ChallengeCompletion[]>) => {
        console.log(response);
        if (response.message) {
          const challenges: ChallengeCompletion[] = response.message;
          let totalSteps = 0;
          challenges.forEach((completion: ChallengeCompletion) => {
            totalSteps = completion.steps;
          });
          this.getHealthData(totalSteps);
        }
      }
    );
  }
  /**
   * Get user steps from today
   */
  getHealthData(totalSteps: number) {
    const todayMidnight: Date = new Date();
    todayMidnight.setHours(0, 0, 0, 0);

    this.health
      .query({
        startDate: new Date(todayMidnight.getTime()),
        endDate: new Date(),
        dataType: 'steps',
        limit: 1000,
      })
      .then((data) => {
        if (data && data.length > 0 && totalSteps > 0) {
          let stepValue = 0;
          for (const healthData of data) {
            stepValue += parseInt(healthData.value, 10);
          }
          // get percent from user steps and total challenge steps
          this.showStepsProgress(stepValue, totalSteps);
        } else {
          // there isn't steps data yet. Hide progress.
          this.hideStepsProgress();
        }
      })
      .catch((error) => console.log('Data error: ' + error));
  }
  /**
   * Hide steps progress component
   */
  hideStepsProgress() {
    this.innerStrokeColor = '#ffffff';
    this.outerStrokeColor = '#ffffff';
  }
  /**
   * Get a percentage from user steps done
   * and show it in the progress component
   */
  showStepsProgress(userSteps: number, totalSteps: number) {
    this.progressPercent = Math.round((userSteps * 100) / totalSteps);

    if (this.progressPercent < 100) {
      this.innerStrokeColor = '#d8e9eb';
      this.outerStrokeColor = '#64d2dc';
    } else {
      this.innerStrokeColor = '#bce8c8';
      this.outerStrokeColor = '#32a852';
    }
  }
  /**
   * Get pending challenges to complete by user's relatives
   */
  getRelativesPendingChallenges() {
    this.apiService.getRelativesPendingChallenges(this.childId).subscribe(
      (result: ApiResponse<ChallengeCompletion[]>) => {
        if (result.status == 200) {
          const relativeChallenges = result.message;
          if (relativeChallenges.length > 0) {
            this.havePendingChallenges = true;
            this.relativesPendingCount = relativeChallenges.length;
          } else {
            this.havePendingChallenges = false;
          }
        }
      }
    );
  }
  /**
   * Show an alert to say to user that their family have pending challenges
   */
  async showPendingChallengesAlert() {
    const alert = await this.alertController.create({
      header: this.translateService.instant('HOME.PENDING_CHALLENGES'),
      message: this.translateService.instant('HOME.FAMILY_CHALLENGES'),
      buttons: [
        {
          text: this.translateService.instant('VERBS.ACCEPT')
        }
      ]
    });

    await alert.present();
  }
}

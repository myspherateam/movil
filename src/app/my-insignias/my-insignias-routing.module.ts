import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MyInsigniasPage } from './my-insignias.page';

const routes: Routes = [
  {
    path: '',
    component: MyInsigniasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MyInsigniasPageRoutingModule {}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MyInsigniasPage } from './my-insignias.page';

describe('MyInsigniasPage', () => {
  let component: MyInsigniasPage;
  let fixture: ComponentFixture<MyInsigniasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyInsigniasPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MyInsigniasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MyInsigniasPageRoutingModule } from './my-insignias-routing.module';

import { MyInsigniasPage } from './my-insignias.page';
import { TranslateModule } from '@ngx-translate/core';
import { PipesModule } from '../pipes/pipes-module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MyInsigniasPageRoutingModule,
    TranslateModule,
    PipesModule
  ],
  declarations: [MyInsigniasPage],
})
export class MyInsigniasPageModule { }

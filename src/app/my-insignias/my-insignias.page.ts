import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { FirebaseX } from '@ionic-native/firebase-x/ngx';
import { Badge } from '../models/badge';
import { ApiService } from '../api/api-service';
import { ApiResponse } from '../models/api-response';
import { LocalStorageService } from 'src/services/storage-service';
import { HttpErrorResponse } from '@angular/common/http';
import { UserBadges } from '../models/user-badges';

@Component({
  selector: 'app-my-insignias',
  templateUrl: './my-insignias.page.html',
  styleUrls: ['./my-insignias.page.scss'],
})
export class MyInsigniasPage implements OnInit {

  public badges: Badge[];
  public futureBadges: Badge[];

  constructor(
    private firebase: FirebaseX,
    private menuController: MenuController,
    private apiService: ApiService,
    private localStorage: LocalStorageService<any>
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.menuController.enable(true);
    this.getMyBadges();
  }

  ionViewDidEnter() {
    const screenName = 'my-insignias';
    this.firebase.logEvent('select_content', { content_type: 'page_view', item_name: screenName });
    this.firebase.setScreenName(screenName);
  }

  getMyBadges() {
    if (this.localStorage.containsKey('user')) {
      const user = this.localStorage.getFromLocalStorage('user');
      let userId = user.id;
      let childId: string;

      if (this.localStorage.containsKey('childId')) {
        childId = this.localStorage.getFromLocalStorage('childId');
        userId = childId;
      }

      this.apiService.getUserBadges(userId, childId).subscribe(
        (result: ApiResponse<UserBadges>) => {
          if (result.status == 200) {
            if (result.message.badges) this.badges = result.message.badges;
            if (result.message.futureBadges) this.futureBadges = result.message.futureBadges;
          }
        },
        (error: HttpErrorResponse) => console.log(error)
      );
    }
  }

}

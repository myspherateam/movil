import { NgModule } from "@angular/core";
import { AuthImagePipe } from "./auth-image-pipe";
import { SafeHtml } from "./safe-html";

@NgModule({
    declarations: [
        AuthImagePipe,
        SafeHtml
    ],
    exports: [
        AuthImagePipe,
        SafeHtml
    ]
})
export class PipesModule { }
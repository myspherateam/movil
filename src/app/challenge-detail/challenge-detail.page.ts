import { Component, OnInit } from '@angular/core';
import { AlertController, Platform, NavController, AnimationController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator/ngx';

import { ChallengeType } from '../models/challenge-type.model';
import { ApiService } from '../api/api-service';
import { User } from '../models/user';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import { Health } from '@ionic-native/health/ngx';
import { StepNames } from '../models/step-names';
import { ChallengeCompletion } from '../models/challenge-completion';
import {
  GoogleMaps,
  GoogleMap,
  MyLocation,
  ILatLng,
  LatLngBounds,
  Poly
} from '@ionic-native/google-maps/ngx';
import { LocalStorageService } from 'src/services/storage-service';
import { Constants } from '../utils/constants';
import { Events } from 'src/services/events-service';
import { FirebaseX } from '@ionic-native/firebase-x/ngx';
import { TranslateService } from '@ngx-translate/core';
import { ApiResponse } from '../models/api-response';

@Component({
  selector: 'app-challenge-detail',
  templateUrl: './challenge-detail.page.html',
  styleUrls: ['./challenge-detail.page.scss'],
})
export class ChallengeDetailPage implements OnInit {
  public challengeId: string;
  public challenge: ChallengeCompletion;
  public challengeTagType: string;
  public challengeType = ChallengeType;
  public user: User;
  public isParent: boolean;
  public haveToAuthorize: boolean;
  public stepsDone: string;
  public arePermissionsAuthorized: boolean;
  public challengeCategory: string;
  public challengeCompleted: boolean;
  public map: GoogleMap;
  public polygon: ILatLng[] = [];
  public isOnTime: boolean;
  public hasChallengeStarted: boolean;
  private stepNames: Map<string, string>;

  constructor(
    public router: Router,
    public route: ActivatedRoute,
    public alertController: AlertController,
    public challengesService: ApiService,
    private launchNavigator: LaunchNavigator,
    private androidPermissions: AndroidPermissions,
    private locationAccuracy: LocationAccuracy,
    private health: Health,
    private platform: Platform,
    private localStorage: LocalStorageService<any>,
    private navController: NavController,
    private firebase: FirebaseX,
    private events: Events,
    private translateService: TranslateService,
    private animationController: AnimationController
  ) {
    this.stepsDone = '';
    this.challengeCompleted = false;
    this.route.queryParams.subscribe((params) => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.challengeId = this.router.getCurrentNavigation().extras.state.challenge;
      }
    });
    this.stepNames = new StepNames(this.translateService).names;
  }

  ngOnInit() {
    this.getChallengeDetail();
  }

  ionViewDidEnter() {
    const screenName = 'challenge-detail';
    this.firebase.logEvent('select_content', {
      content_type: 'page_view',
      item_name: screenName,
    });
    this.firebase.setScreenName(screenName);
  }

  getChallengeDetail() {
    let childId: string;
    if (this.localStorage.containsKey('childId')) {
      childId = this.localStorage.getFromLocalStorage('childId');
    }
    this.challengesService
      .getCompletionChallengeDetail(this.challengeId, childId)
      .subscribe(
        (result) => {
          if (result.status === 200) {
            this.challenge = result.message;
            console.log(this.challenge);
            this.user = this.localStorage.getFromLocalStorage('user');
            console.log(this.localStorage.getFromLocalStorage('childId'));
            this.isParent =
              this.localStorage.getFromLocalStorage('childId') === undefined;
            this.haveToAuthorize = this.checkHaveToAuthorize();
            this.initDetail();
          }
        },
        (error) => {
          console.log('Error challenge detail: ' + error);
        }
      );
  }

  initDetail() {
    if (this.challenge) {
      this.isOnTime = this.challengeIsOnTime();
      switch (this.challenge.type) {
        case this.challengeType.gps:
          this.loadMap();
          break;
        case this.challengeType.physical:
          if (this.isOnTime) {
            this.getSteps();
          }
          break;
      }
      this.getChallengeTypeTag();
      this.challengeCategory = this.stepNames.get(this.challenge.category);
      this.hasChallengeStarted = this.checkChallengeHasStarted();
    }
  }

  // get challenge title
  getChallengeTypeTag() {
    if (this.challenge) {
      switch (this.challenge.type) {
        case this.challengeType.authority:
          this.challengeTagType = this.translateService.instant('CHALLENGES_LIST.AUTHORITY');
          break;
        case this.challengeType.gps:
          this.challengeTagType = this.translateService.instant('CHALLENGES_LIST.GPS');
          break;
        case this.challengeType.news:
          this.challengeTagType = this.translateService.instant('CHALLENGES_LIST.NEWS');
          break;
        case this.challengeType.physical:
          this.challengeTagType = this.translateService.instant('CHALLENGES_LIST.PHYSICAL_ACTIVITY');
          break;
        case this.challengeType.questionnaire:
          this.challengeTagType = this.translateService.instant('CHALLENGES_LIST.QUESTIONNAIRE');
          break;
      }
    }
  }
  /**
   * Get a tag for the user who is assigned to the challenge
   */
  getChallengeFor(challengeFor: string): string {
    return challengeFor === 'child' ?
      this.translateService.instant('CHALLENGE_DETAIL.CHILDREN') :
      this.translateService.instant('CHALLENGE_DETAIL.PARENTS');
  }
  /**
   * Check if a challenge has started but hasn't finished yet
   */
  challengeIsOnTime(): boolean {
    const finishDatetime = this.getFormattedDateInMillis(
      this.challenge.periodFinishDatetime
    );
    const startDatetime = this.getFormattedDateInMillis(
      this.challenge.periodStartDatetime
    );
    const actualDatetime = new Date().getTime();
    return actualDatetime >= startDatetime && actualDatetime < finishDatetime;
  }
  /**
   * Check if a challenge has started or it's starting in several days
   */
  checkChallengeHasStarted(): boolean {
    const startDatetime = this.getFormattedDateInMillis(
      this.challenge.periodStartDatetime
    );
    console.log('start millis ' + startDatetime);
    const actualDatetime = new Date().getTime();
    return actualDatetime >= startDatetime;
  }

  getFormattedDateInMillis(dateString: string): number {
    return new Date(dateString).getTime();
  }

  markAChallengeAsCompleted(challengeComplete: ChallengeCompletion) {
    const finishDatetime = this.getFormattedDateInMillis(
      this.challenge.periodFinishDatetime
    );
    const startDatetime = this.getFormattedDateInMillis(
      this.challenge.periodStartDatetime
    );
    const actualDatetime = new Date().getTime();
    console.log(
      'tiempo de finalización: ' +
      finishDatetime +
      ' tiempo actual: ' +
      actualDatetime
    );

    if (actualDatetime >= startDatetime && actualDatetime < finishDatetime) {

      let childId: string;
      if (this.localStorage.containsKey('childId')) {
        childId = this.localStorage.getFromLocalStorage('childId');
      }

      const lastAwardedPoints = this.challenge.awardedPoints;

      this.challengesService
        .markAChallengeAsComplete(challengeComplete, childId)
        .subscribe(
          (result) => {
            if (result.status === 200) {
              const challengeCompletion: ChallengeCompletion = result.message;
              const pointsSaved = challengeCompletion.awardedPoints - lastAwardedPoints;

              this.showAlertDialog(
                this.translateService.instant('CHALLENGE_DETAIL.DONE'),
                this.translateService.instant('CHALLENGE_DETAIL.CHALLENGE_DONE') +
                pointsSaved +
                this.translateService.instant('CHALLENGE_DETAIL.POINTS'),
                this.translateService.instant('CHALLENGE_DETAIL.CHALLENGE_OVERCOME')
              );
            }
          },
          (error) => {
            console.log(error);
          }
        );
    }
  }

  async showAlertDialog(header: string, subHeader: string, message: string) {
    const alert = await this.alertController.create({
      header,
      subHeader,
      message,
      buttons: [
        {
          text: this.translateService.instant('VERBS.ACCEPT'),
          handler: () => {
            // go back
            this.challengeCompleted = true;
            this.events.publish(Constants.CHALLENGE_COMPLETED, {});
            this.navController.back();
          },
        },
      ],
      cssClass: 'completion-modal',
      enterAnimation: (baseEl: any, opts?: any) => {
        return (this.animationController
          .create()
          .addElement(baseEl.querySelector('.alert-wrapper'))
          .duration(1000)
          .keyframes([
            { offset: 0, transform: 'scale(1)', opacity: '1' },
            { offset: 0.5, transform: 'scale(1.2)', opacity: '0.3' },
            { offset: 1, transform: 'scale(1)', opacity: '1' }
          ]));
      }
    });

    await alert.present();
  }

  // launch questionnaire detail
  goToQuestionnaire() {
    this.router.navigateByUrl('/challenge-questionnaire', {
      state: { challenge: this.challenge },
    });
  }

  // launch news detail
  goToNews() {
    this.router.navigateByUrl('/challenge-news', {
      state: { challenge: this.challenge },
    });
  }

  /**
   * Challenge physical activity
   */

  async getSteps() {
    // check platform ready
    const ready = !!(await this.platform.ready());
    if (ready && this.platform.is('cordova')) {
      // check plugin is ready
      this.checkHealthApp();
    }
  }

  checkHealthApp() {
    this.health
      .isAvailable()
      .then((available: boolean) => {
        // request activity recognition permission for android 10
        this.androidPermissions.requestPermission('android.permission.ACTIVITY_RECOGNITION')
          .then(result => {
            console.log('Activity recognition permission: ' + result.hasPermission);
            // Authorize using the requestAuthorization method in health plugin
            this.requestHealthPermissions();
          })
          .catch(console.error);
      })
      .catch((error) => console.log(error));
  }

  requestHealthPermissions() {
    this.health
      .requestAuthorization([{ read: ['steps'] }])
      .then((result) => {
        console.log('Steps authorization: ' + result);
        this.arePermissionsAuthorized = true;
        this.getHealthData();
      })
      .catch((error) => {
        this.arePermissionsAuthorized = false;
        console.log('Steps authorization error: ' + error);
      });
  }

  getHealthData() {
    const todayMidnight: Date = new Date();
    todayMidnight.setHours(0, 0, 0, 0);

    this.health
      .query({
        // data from today steps
        startDate: new Date(todayMidnight.getTime()),
        endDate: new Date(),
        dataType: 'steps',
        limit: 1000,
        filtered: true
      })
      .then((data) => {
        console.log(data);
        if (data && data.length > 0) {
          let stepValue = 0;

          for (const healthData of data) {
            stepValue += parseInt(healthData.value, 10);
          }
          this.stepsDone = stepValue.toString();
          this.checkPhysicalChallengeDone(stepValue);
        } else {
          this.stepsDone = this.translateService.instant('CHALLENGE_DETAIL.NO_DATA');
        }
      })
      .catch((error) => console.log('Data error: ' + error));
  }
  /**
   * Check if the user can complete a physical challenge.
   * Only if the user overcome the steps proposed, it's assigned
   * to the challenge and isn't a parent that enter like a child.
   * @param stepValue steps done by the user
   */
  checkPhysicalChallengeDone(stepValue: number) {
    if (this.challenge) {
      // check if there is a childId, so the user is a parent that enter like a child
      let childId: string;
      if (this.localStorage.containsKey('childId')) {
        childId = this.localStorage.getFromLocalStorage('childId');
      }
      if (
        stepValue >= this.challenge.steps &&
        this.challenge.important &&
        !childId
      ) {
        const challengeCompletion = new ChallengeCompletion();
        challengeCompletion.id = this.challenge.id;
        challengeCompletion.userSteps = stepValue;
        this.markAChallengeAsCompleted(challengeCompletion);
      }
    }
  }
  /**
   * Refresh steps count
   */
  refreshSteps() {
    this.getSteps();
  }

  /**
   * Challenge GPS
   */
  /**
   * set polygon on map from challenge coordinates
   */
  async loadMap() {
    const ready = !!(await this.platform.ready());

    if (ready) {
      // GeoJson Polygon: first element is the polygon itself, next elements are the holes inside the polygon
      const holes: ILatLng[][] = [];

      this.challenge.location.coordinates.forEach((values, index) => {
        const polygonValues: ILatLng[] = [];

        values.forEach((element) => {
          // get the coordinates inside
          // pass the number points to ILatLng model
          const polygonPoint: ILatLng = {
            lat: element[1],
            lng: element[0],
          };

          polygonValues.push(polygonPoint);
        });

        if (index > 0) {
          holes.push(polygonValues);
        } else {
          this.polygon.push(...polygonValues);
        }
      });

      this.map = GoogleMaps.create('map', {
        gestures: {
          scroll: true,
          tilt: false,
          zoom: true,
          rotate: true,
        },
        controls: {
          compass: false,
        },
      });

      this.map.moveCamera({
        target: this.polygon,
        zoom: 10,
        tilt: 30
      });

      this.map.addPolygonSync({
        points: this.polygon,
        strokeColor: '#ff4b00',
        fillColor: '#ffa680',
        strokeWidth: 5,
        holes,
      });

    }
  }

  // launch map
  goToMap(isNavigation: boolean) {
    // destination direction
    this.checkGPSPermissions(isNavigation);
  }

  getMyLocation(isNavigation: boolean) {
    this.checkGPSPermissions(isNavigation);
  }

  checkGPSPermissions(isNavigation: boolean) {
    this.androidPermissions
      .checkPermission(
        this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION
      )
      .then(
        (result) => {
          if (result.hasPermission) {
            // if has permissions show 'Turn on GPS' alert
            this.askTurnOnGPS(isNavigation);
          } else {
            // if there isn´t permissions, request them.
            this.requestGPSPermission(isNavigation);
          }
        },
        (error) => {
          console.log(error);
        }
      );
  }

  requestGPSPermission(isNavigation: boolean) {
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {
      if (canRequest) {
        console.log(canRequest);
      } else {
        this.androidPermissions
          .requestPermission(
            this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION
          )
          .then(
            () => {
              // permission granted, ask for turn on gps
              this.askTurnOnGPS(isNavigation);
            },
            (error) => {
              // permission rejected
              console.log('Error permissions: ' + error);
            }
          );
      }
    });
  }

  askTurnOnGPS(isNavigation: boolean) {
    this.locationAccuracy
      .request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY)
      .then(
        () => {
          this.getLocationCoordinates(isNavigation);
        },
        (error) => {
          // permission rejected
          console.log('Error permissions: ' + error);
        }
      );
  }

  getLocationCoordinates(isNavigation: boolean) {
    this.map
      .getMyLocation()
      .then((location: MyLocation) => {
        console.log(location);
        if (isNavigation) {
          this.launchMapNavigation();
        } else {
          this.showMyPositionOnMap(location);
        }
      })
      .catch((error) => console.log('Error getting my location: ' + error));
  }

  showMyPositionOnMap(myLocation: MyLocation) {
    const isInPolygon: boolean = Poly.containsLocation(
      myLocation.latLng,
      this.polygon
    );
    console.log(
      'Is inside the polygon?: ' +
      isInPolygon +
      ' precision: ' +
      myLocation.accuracy
    );

    this.map.animateCamera({
      target: myLocation.latLng,
      zoom: 18,
      tilt: 30,
      duration: 1000,
    });

    this.map.addMarkerSync({
      position: myLocation.latLng,
    });

    if (isInPolygon && this.challenge.important) {
      // challenge passed
      const challengeCompletion = new ChallengeCompletion();
      challengeCompletion.id = this.challenge.id;
      challengeCompletion.userLocation = {
        type: 'Point',
        coordinates: [myLocation.latLng.lng, myLocation.latLng.lat],
      };
      challengeCompletion.precision = myLocation.accuracy;
      this.markAChallengeAsCompleted(challengeCompletion);
    }
  }

  launchMapNavigation() {
    const options: LaunchNavigatorOptions = {
      appSelection: {
        dialogHeaderText: this.translateService.instant('CHALLENGE_DETAIL.SELECT_MAPS_APP'),
        cancelButtonText: this.translateService.instant('VERBS.CANCEL'),
        rememberChoice: {
          enabled: false,
        },
      },
    };
    const center: string = this.getCenterPointFromPolygon();
    this.launchNavigator.navigate(center, options);
  }

  getCenterPointFromPolygon(): string {
    const bounds = new LatLngBounds();
    for (const coordinates of this.polygon) {
      bounds.extend(coordinates);
    }
    return bounds.getCenter().lat + ',' + bounds.getCenter().lng;
  }

  /**
   * Challenge authority
   */

  // check if user is a parent and has to authorize the challenge
  checkHaveToAuthorize(): boolean {
    let authorize = false;
    console.log(this.challenge.type);
    console.log(this.user.roles);
    console.log(this.challenge.important);
    // check if challenge is authority type and user is the parent of the user is assigned to
    if (this.challenge && this.user) {
      if (
        this.challenge.type === this.challengeType.authority &&
        this.user.roles.includes('Parent') &&
        this.isParent
      ) {
        authorize = true;
      }
    }

    return authorize;
  }

  // mark authority challenge to done
  authorityChallengeDone(authorizeAChild: boolean) {
    const challengeCompletion = new ChallengeCompletion();
    challengeCompletion.id = this.challenge.id;
    challengeCompletion.approvedById = this.user.id;
    challengeCompletion.completedByParents = [{ parentId: this.user.id }];
    if (authorizeAChild) {
      challengeCompletion.forId = this.challenge.forId;
    }
    this.markAChallengeAsCompleted(challengeCompletion);
  }

  /**
   * Check if an authority challenge, assigned for a parent, has been completed at least by one parent
   */
  authorityIsCompletedByParents(): boolean {
    return (
      this.challenge.assignedFor &&
      this.challenge.assignedFor.includes('parent') &&
      this.challenge.completedByParents.length === 0
    );
  }
  /**
   * Check if an authority challenge, assigned for a child, has been completed by the child
   */
  authorityIsCompletedByChild(): boolean {
    return (
      this.challenge.assignedFor &&
      this.challenge.assignedFor.includes('child') &&
      !this.challenge.completed
    );
  }
}

export class Constants {
    public static readonly CHALLENGE_COMPLETED = 'challenge:completed';
    public static readonly COUNT_CHANGED = 'count_changed:completed';
    public static readonly REFRESH_TOKEN = 'refresh_token:expired';
    public static readonly ROLE_CHANGE = 'role:changed';
}

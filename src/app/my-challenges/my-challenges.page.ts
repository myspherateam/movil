import { Component, OnInit, ViewChild } from '@angular/core';
import { ChallengeCompletion } from 'src/app/models/challenge-completion';
import { LocalStorageService } from 'src/services/storage-service';
import { ApiService } from 'src/app/api/api-service';
import { ApiResponse } from 'src/app/models/api-response';
import { NavigationExtras, Router } from '@angular/router';
import { IonSelect, MenuController } from '@ionic/angular';
import { FirebaseX } from '@ionic-native/firebase-x/ngx';

@Component({
  selector: 'app-my-challenges',
  templateUrl: './my-challenges.page.html',
  styleUrls: ['./my-challenges.page.scss'],
})
export class MyChallengesPage implements OnInit {

  public challengeSubcategory: string;
  public challengeCategory: string;
  public isLoading: boolean;
  public challenges: ChallengeCompletion[];
  public allChallenges: ChallengeCompletion[];
  public type = 'all';
  public finished = false;
  public completed = false;

  @ViewChild('challengeTypeFilter', { static: false }) challengeTypeSelect: IonSelect;

  constructor(
    private localStorage: LocalStorageService<any>,
    private apiService: ApiService,
    private router: Router,
    private firebase: FirebaseX,
    private menuController: MenuController
  ) { }

  ngOnInit() { }

  ionViewDidEnter() {
    const screenName = 'my-challenges';
    this.firebase.logEvent('select_content', { content_type: 'page_view', item_name: screenName });
    this.firebase.setScreenName(screenName);
  }

  ionViewWillEnter() {
    this.menuController.enable(true);
    this.getChallenges();
  }

  getChallenges(type: string = null) {
    let childId: string;
    if (this.localStorage.containsKey('childId')) {
      childId = this.localStorage.getFromLocalStorage('childId');
    }

    this.apiService.getCompletionChallenges(
      null,
      null,
      type,
      this.finished,
      childId,
      this.completed
    ).subscribe(
      (response: ApiResponse<ChallengeCompletion[]>) => {
        if (response.status === 200) {
          this.allChallenges = response.message;
          this.challenges = response.message;
          this.isLoading = false;
        }
      },
      error => console.log('Error challenges: ' + error)
    );
  }

  onChallengeClick(challenge: ChallengeCompletion) {
    // go to challenge detail
    const navigationExtras: NavigationExtras = {
      state: {
        challenge: challenge.id
      }
    };

    this.router.navigateByUrl('/challenge-detail', navigationExtras);
  }

  onSearch(ev: CustomEvent) {
    this.isLoading = true;
    // filter list
    const value = ev.detail.value;

    if (value) {
      if (value.trim !== '') {
        this.challenges = this.allChallenges.filter(term => {
          return term.title.toLowerCase().indexOf(value.trim().toLowerCase()) > -1;
        });
      }
      this.isLoading = false;
    } else {
      this.challenges = this.allChallenges;
      this.isLoading = false;
    }
  }

  filterList(ev: CustomEvent) {
    // get filter list selected
    this.completed = false;
    this.finished = false;
    const listType = ev.detail.value;
    this.finished = listType === 'past' ? true : false;
    this.completed = listType === 'completed' ? true : false;

    // reload data with new filters
    this.getChallenges(this.type);
  }

  onFiltersClick() {
    this.challengeTypeSelect.open();
  }

  filterByType(ev: CustomEvent) {
    // filter by challenge type
    this.type = ev.detail.value;
    if (this.type === 'all') { this.type = null; }
    this.getChallenges(this.type);
  }

}

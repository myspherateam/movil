import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MyChallengesPageRoutingModule } from './my-challenges-routing.module';

import { MyChallengesPage } from './my-challenges.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MyChallengesPageRoutingModule,
    TranslateModule
  ],
  declarations: [MyChallengesPage]
})
export class MyChallengesPageModule { }

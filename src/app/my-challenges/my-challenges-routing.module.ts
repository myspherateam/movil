import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MyChallengesPage } from './my-challenges.page';

const routes: Routes = [
  {
    path: '',
    component: MyChallengesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MyChallengesPageRoutingModule {}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorageService } from 'src/services/storage-service';
import { MenuController, AlertController } from '@ionic/angular';
import { FirebaseX } from '@ionic-native/firebase-x/ngx';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {

  constructor(
    private router: Router,
    private localStorage: LocalStorageService<any>,
    private menuController: MenuController,
    private firebase: FirebaseX,
    private alertController: AlertController,
    private translateService: TranslateService
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter() {
    const screenName = 'settings';
    this.firebase.logEvent('select_content', { content_type: 'page_view', item_name: screenName });
    this.firebase.setScreenName(screenName);
  }

  ionViewWillEnter() {
    this.menuController.enable(true);
  }

  async showAlertSignOut() {
    const alert = await this.alertController.create({
      header: this.translateService.instant('SETTINGS.SIGN_OUT'),
      message: this.translateService.instant('SETTINGS.SIGN_OUT_DIALOG_MESSAGE'),
      buttons: [
        {
          text: this.translateService.instant('VERBS.CANCEL')
        },
        {
          text: this.translateService.instant('VERBS.ACCEPT'),
          handler: () => {
            this.signOut();
          }
        }
      ]
    });

    await alert.present();
  }

  signOut() {
    this.localStorage.signOut();
    this.router.navigateByUrl('/register');
  }

  showAbout() {
    this.router.navigateByUrl('/about');
  }

}

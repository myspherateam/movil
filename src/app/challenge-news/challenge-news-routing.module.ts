import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChallengeNewsPage } from './challenge-news.page';

const routes: Routes = [
  {
    path: '',
    component: ChallengeNewsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChallengeNewsPageRoutingModule {}

export interface Delta {
    ops: any[];
}

export const emptyDelta: Delta = {
    ops: [
        {
            insert: '\n'
        }
    ]
};

export const dummyDelta: Delta = {
    ops: [
        {
            insert: 'DummyDelta\n'
        }
    ]
};

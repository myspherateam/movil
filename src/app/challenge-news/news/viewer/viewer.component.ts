import { Component, OnInit, Input } from '@angular/core';
import { Delta } from '../interfaces/delta';
// Needed to actually run the code that injects the extensions into quill
import '../quill-extensions/Load';

@Component({
  selector: 'news-viewer',
  templateUrl: './viewer.component.html',
  styleUrls: ['./viewer.component.scss'],
})
export class ViewerComponent implements OnInit {
  @Input() content: Delta;
  constructor() { }

  ngOnInit() {}

}

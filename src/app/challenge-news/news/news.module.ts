import { FormsModule } from '@angular/forms';
import { ViewerComponent } from './viewer/viewer.component';
import { EditorComponent } from './editor/editor.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuillModule } from 'ngx-quill';



@NgModule({
  declarations: [EditorComponent, ViewerComponent],
  imports: [
    CommonModule,
    FormsModule,
    QuillModule.forRoot()
  ],
  exports: [
    EditorComponent,
    ViewerComponent
  ]
})
export class NewsModule { }

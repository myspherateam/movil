import Quill from 'quill';

const BaseImage = Quill.import('formats/image');

const ATTRIBUTES = [
  'alt',
  'height',
  'width',
  'style'
];

const WHITE_STYLE = ['margin', 'display', 'float'];

export class ImageBlot extends BaseImage {
  static formats(domNode) {
    return ATTRIBUTES.reduce((formats, attribute) => {
      if (domNode.hasAttribute(attribute)) {
        formats[attribute] = domNode.getAttribute(attribute);
      }
      return formats;
    }, {});
  }

  format(name, value) {
    if (ATTRIBUTES.indexOf(name) > -1) {
      const domNode = 'domNode';
      if (value) {
        if (name === 'style') {
          value = this.sanitize_style(value);
        }
        this[domNode].setAttribute(name, value);
      } else {
        this[domNode].removeAttribute(name);
      }
    } else {
      super.format(name, value);
    }
  }

  sanitize_style(style) {
    const styleArr = style.split(';');
    let allowStyle = '';
    styleArr.forEach((v, i) => {
      if (WHITE_STYLE.indexOf(v.trim().split(':')[0]) !== -1) {
        allowStyle += v + ';';
      }
    });
    return allowStyle;
  }
}

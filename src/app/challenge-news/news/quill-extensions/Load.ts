import { ImageBlot } from './ImageBlot';
import ImageResize from 'quill-image-resize';
import { ImageDrop } from 'quill-image-drop-module';
import Quill from 'quill';

Quill.register('modules/imageResize', ImageResize);
Quill.register('modules/ImageDrop', ImageDrop);
Quill.register(ImageBlot, true);

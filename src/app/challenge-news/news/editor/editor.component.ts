import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { Delta } from '../interfaces/delta';
import { QuillEditorComponent } from 'ngx-quill';
// Needed to actually run the code that injects the extensions into quill
import '../quill-extensions/Load';

@Component({
  selector: 'news-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss'],
})
export class EditorComponent implements OnInit {
  @ViewChild('editor', {static: true}) editor: QuillEditorComponent;

  @Input() styles: any = {};
  @Input() toolbar: any[] = [];
  @Input() placeholder: string;

  @Output() contentChange: EventEmitter<Delta> = new EventEmitter<Delta>();

  private contentValue: Delta;
  modules: any = {};


  constructor() {
    this.modules = {
      imageResize: {},
      imageDrop: true
    };
  }

  ngOnInit() {
  }

  @Input()
  get content(): Delta {
    return this.contentValue;
  }

  set content(content: Delta) {
    this.contentValue = content;
  }

  onType(event: Delta) {
    this.contentValue = event;
    this.contentChange.emit(this.contentValue);
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ChallengeNewsPageRoutingModule } from './challenge-news-routing.module';

import { ChallengeNewsPage } from './challenge-news.page';
import { NewsModule } from './news/news.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChallengeNewsPageRoutingModule,
    NewsModule,
    TranslateModule
  ],
  declarations: [ChallengeNewsPage]
})
export class ChallengeNewsPageModule {}

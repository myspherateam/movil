import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, AnimationController, MenuController, NavController } from '@ionic/angular';
import { ChallengeCompletion } from '../models/challenge-completion';
import { Delta } from './news/interfaces/delta';
import { FirebaseX } from '@ionic-native/firebase-x/ngx';
import { Events } from 'src/services/events-service';
import { Constants } from '../utils/constants';
import { ApiService } from '../api/api-service';
import { LocalStorageService } from 'src/services/storage-service';
import { TranslateService } from '@ngx-translate/core';
import { ApiResponse } from '../models/api-response';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-challenge-news',
  templateUrl: './challenge-news.page.html',
  styleUrls: ['./challenge-news.page.scss'],
})

export class ChallengeNewsPage implements OnInit, OnDestroy {

  public challenge: ChallengeCompletion;
  public newsContent: Delta;
  public isOnTime: boolean;

  constructor(
    private router: Router,
    private firebase: FirebaseX,
    public route: ActivatedRoute,
    private menuController: MenuController,
    private events: Events,
    private navController: NavController,
    public apiService: ApiService,
    public localStorage: LocalStorageService<any>,
    public alertController: AlertController,
    private translateService: TranslateService,
    private animationController: AnimationController
  ) {
    this.menuController.enable(false);
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.challenge = this.router.getCurrentNavigation().extras.state.challenge;
        this.newsContent = JSON.parse(this.challenge.news);
      }
    });
  }

  ngOnInit() {
    this.events.subscribe(Constants.CHALLENGE_COMPLETED, () => {
      setTimeout(() => {
        this.navController.pop();
      }, 0);
    });
    this.isOnTime = this.challengeIsOnTime();
  }

  ionViewDidEnter() {
    const screenName = 'challenge-news';
    this.firebase.logEvent('select_content', { content_type: 'page_view', item_name: screenName });
    this.firebase.setScreenName(screenName);
  }

  ngOnDestroy(): void {
    this.events.destroy(Constants.CHALLENGE_COMPLETED);
  }
  /**
   * Check if a challenge has started but hasn't finished yet
   */
  challengeIsOnTime(): boolean {
    const finishDatetime = this.getFormattedDateInMillis(
      this.challenge.periodFinishDatetime
    );
    const startDatetime = this.getFormattedDateInMillis(
      this.challenge.periodStartDatetime
    );
    const actualDatetime = new Date().getTime();
    return actualDatetime >= startDatetime && actualDatetime < finishDatetime;
  }

  getFormattedDateInMillis(dateString: string): number {
    return new Date(dateString).getTime();
  }
  /**
   * Open questionnaire to complete, if news has one.
   */
  openNewsTest() {
    this.router.navigateByUrl('/challenge-questionnaire', { state: { challenge: this.challenge } });
  }
  /**
   * Complete challenge if user read the news
   */
  completeChallenge() {
    const finishDatetime = new Date(this.challenge.periodFinishDatetime).getTime();
    const startDatetime = new Date(this.challenge.periodStartDatetime).getTime();
    const actualDatetime = new Date().getTime();
    console.log('tiempo de finalización: ' + finishDatetime + ' tiempo actual: ' + actualDatetime);

    if (actualDatetime >= startDatetime && actualDatetime < finishDatetime) {
      let childId: string;
      if (this.localStorage.containsKey('childId')) {
        childId = this.localStorage.getFromLocalStorage('childId');
      }

      const lastAwardedPoints = this.challenge.awardedPoints;

      this.apiService.markAChallengeAsComplete(this.challenge, childId).subscribe(
        (result: ApiResponse<ChallengeCompletion>) => {
          if (result.status === 200) {
            const challengeCompletion: ChallengeCompletion = result.message;
            const pointsSaved = challengeCompletion.awardedPoints - lastAwardedPoints;

            this.showAlertDialog(
              this.translateService.instant('CHALLENGE_DETAIL.DONE'),
              this.translateService.instant('CHALLENGE_DETAIL.CHALLENGE_DONE') +
              pointsSaved +
              this.translateService.instant('CHALLENGE_DETAIL.POINTS'),
              this.translateService.instant('CHALLENGE_DETAIL.CHALLENGE_OVERCOME'),
              'completion-modal',
              true
            );
          }
        },
        (error: HttpErrorResponse) => {
          console.log(error);
          if (error.error) {
            const apiError: ApiResponse<string> = error.error;
            if (apiError.message) {
              this.showAlertDialog(
                this.translateService.instant('ERRORS.TRY_AGAIN'),
                this.translateService.instant('ERRORS.CHALLENGE_FAILED_TITLE'),
                this.translateService.instant('ERRORS.CHALLENGE_FAILED'),
                undefined,
                false
              );
            }
          }
        }
      );
    }
  }

  async showAlertDialog(header: string, subHeader: string, message: string, cssClass: string, animated: boolean) {
    const alert = await this.alertController.create({
      header,
      subHeader,
      message,
      buttons: [{
        text: this.translateService.instant('VERBS.ACCEPT'),
        handler: () => {
          // challenge was completed. Go back to challenges list
          if (!header.includes('Error')) {
            this.events.publish(Constants.CHALLENGE_COMPLETED, {});
            this.navController.pop();
          }
        }
      }],
      cssClass,
      animated,
      enterAnimation: (baseEl: any, opts?: any) => {
        return (this.animationController
          .create()
          .addElement(baseEl.querySelector('.alert-wrapper'))
          .duration(1000)
          .keyframes([
            { offset: 0, transform: 'scale(1)', opacity: '1' },
            { offset: 0.5, transform: 'scale(1.2)', opacity: '0.3' },
            { offset: 1, transform: 'scale(1)', opacity: '1' }
          ]));
      }
    });
    await alert.present();
  }

}

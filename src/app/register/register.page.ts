import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { MenuController, AlertController, ModalController } from '@ionic/angular';
import { ApiService } from '../api/api-service';
import { ApiResponse } from '../models/api-response';
import { Register } from '../models/register';
import { LocalStorageService } from 'src/services/storage-service';
import { HttpErrorResponse } from '@angular/common/http';
import { Market } from '@ionic-native/market/ngx';
import { FirebaseX } from '@ionic-native/firebase-x/ngx';
import { TranslateService } from '@ngx-translate/core';
import { Login } from '../models/login';
import { PrivacyPolicyModalComponent } from './privacy-policy-modal/privacy-policy-modal.component';


@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage {

  public registerCode: string;
  public isAlreadyRegistered: boolean;
  public password: string;

  constructor(
    private router: Router,
    private menuController: MenuController,
    private apiService: ApiService,
    private localStorage: LocalStorageService<any>,
    private alertController: AlertController,
    private market: Market,
    private firebase: FirebaseX,
    private translateService: TranslateService,
    private modalController: ModalController
  ) {
    this.menuController.enable(false);
  }

  ionViewDidEnter() {
    const screenName = 'register';
    this.firebase.logEvent('select_content', { content_type: 'page_view', item_name: screenName });
    this.firebase.setScreenName(screenName);
  }

  async showVersionAlert(needUpdate: boolean, title: string, subtitle: string) {
    let buttons: any[];
    if (needUpdate) {
      buttons = [
        {
          text: this.translateService.instant('VERBS.ACCEPT'),
          handler: () => {
            // go to market
            this.market.open('com.mysphera.paido');
          }
        }
      ];
    } else {
      buttons = [
        {
          text: this.translateService.instant('VERSION.LATER')
        },
        {
          text: this.translateService.instant('VERBS.ACCEPT'),
          handler: () => {
            // go to market
            this.market.open('com.mysphera.paido');
          }
        }
      ];
    }

    const alert = await this.alertController.create({
      header: title,
      message: subtitle,
      backdropDismiss: false,
      buttons
    });

    await alert.present();
  }
  /**
   * User press access button. Check if user has been registered.
   */
  onAccess() {
    // show modal with the privacy policy before register/login
    const privacyAccepted: boolean = this.localStorage.getFromLocalStorage('privacyAccepted');
    if (privacyAccepted) {
      this.launchRegistryCheck();
    } else {
      this.presentPrivacyPolicyModal();
    }
  }
  /**
   * Register user
   */
  registerUser() {
    this.apiService.register(this.registerCode.trim()).subscribe(
      (result: ApiResponse<Register>) => {
        if (result.status === 200) {
          const register: Register = result.message;
          this.localStorage.storeOnLocalStorage('username', register.username);
          this.localStorage.storeOnLocalStorage('password', register.password);
          this.localStorage.storeOnLocalStorage('isAlreadyRegistered', register.isAlreadyRegistered);
          this.isAlreadyRegistered = register.isAlreadyRegistered;

          if (!this.isAlreadyRegistered) {
            this.presentAlert();
          } else {
            // user is already registered alert user
            this.showAlert(
              this.translateService.instant('REGISTER.DIALOG_REGISTERED_TITLE'),
              this.translateService.instant('REGISTER.DIALOG_REGISTERED_SUBTITLE')
            );
          }
        }
      },
      (errorHttp: HttpErrorResponse) => {
        this.showAlert(this.translateService.instant('ERRORS.ERROR'), errorHttp.error.message);
        console.log(errorHttp);
      }
    );
  }
  /**
   * Login a user that was registered. Pass the input password value
   */
  loginRegisteredUser() {
    // user is already registered login user
    const username = this.localStorage.getFromLocalStorage('username');
    this.apiService.login(username, this.password).subscribe(
      (result: Login) => {
        console.log('Login result: ' + result.access_token);
        this.localStorage.storeOnLocalStorage('access_token', result.access_token);
        this.localStorage.storeOnLocalStorage('refresh_token', result.refresh_token);
        this.localStorage.storeOnLocalStorage('password', this.password);

        this.router.navigateByUrl('/login');
      },
      (response: HttpErrorResponse) => {
        console.log(response);
        if (response.status === 401) {
          this.showAlert(
            this.translateService.instant('ERRORS.ERROR'),
            this.translateService.instant('ERRORS.INCORRECT_CREDENTIALS')
          );
        } else {
          this.showAlert(this.translateService.instant('ERRORS.ERROR'), response.message);
        }
      }
    );
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: this.translateService.instant('REGISTER.DIALOG_CREATED_TITLE'),
      subHeader: this.translateService.instant('REGISTER.DIALOG_CREATED_SUBTITLE'),
      message: this.translateService.instant('REGISTER.DIALOG_CREATED_DESC'),
      backdropDismiss: false,
      buttons: [{
        text: this.translateService.instant('VERBS.ACCESS'),
        handler: () => {
          this.router.navigateByUrl('/login');
        }
      }]
    });

    await alert.present();
  }

  async showAlert(header: string, message: string) {
    const alert = await this.alertController.create({
      header,
      message,
      buttons: [{
        text: this.translateService.instant('VERBS.OK')
      }]
    });

    await alert.present();
  }
  /**
   * Show modal with the privacy policy to accept before register/login
   */
  async presentPrivacyPolicyModal() {
    const modal = await this.modalController.create({
      component: PrivacyPolicyModalComponent,
      cssClass: 'privacy-modal',
      backdropDismiss: false
    });
    modal.onDidDismiss().then(result => {
      if (result.data && result.data === 'accept') {
        this.localStorage.storeOnLocalStorage('privacyAccepted', true);
        this.launchRegistryCheck();
      }
    });
    return await modal.present();
  }
  /**
   * Check if user has been registered or not and launch login or register
   */
  launchRegistryCheck() {
    if (!this.isAlreadyRegistered) {
      // register user and check if is already registered
      this.registerUser();
    } else {
      this.loginRegisteredUser();
    }
  }

}

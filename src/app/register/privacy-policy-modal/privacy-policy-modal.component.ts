import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-privacy-policy-modal',
  templateUrl: './privacy-policy-modal.component.html',
  styleUrls: ['./privacy-policy-modal.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class PrivacyPolicyModalComponent implements OnInit {

  public privacyAccepted: boolean;

  constructor(
    private modalController: ModalController
  ) { }

  ngOnInit() { }
  /**
   * Cancel button. Dismiss dialog.
   */
  cancel() {
    this.modalController.dismiss('cancel');
  }
  /**
   * Accept button. Dismiss dialog passing identificator tag
   */
  accept() {
    this.modalController.dismiss('accept');
  }
}

import { NgModule, LOCALE_ID } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ApiService } from './api/api-service';
import { LaunchNavigator } from '@ionic-native/launch-navigator/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import { Health } from '@ionic-native/health/ngx';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LocalStorageService } from 'src/services/storage-service';
import { registerLocaleData } from '@angular/common';
import localeEs from '@angular/common/locales/es';
import { GoogleMaps } from '@ionic-native/google-maps/ngx';
import { AuthenticatorInterceptor } from './api/authenticator-interceptor';
import { JwtModule, JWT_OPTIONS } from '@auth0/angular-jwt';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { Market } from '@ionic-native/market/ngx';
import { FirebaseX } from '@ionic-native/firebase-x/ngx';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { HttpLoaderFactory, HttpClientTranslator } from './api/http-client-translator';
import {InAppBrowser} from '@ionic-native/in-app-browser/ngx';

registerLocaleData(localeEs, 'es');

export function jwtOptionsFactory(storage) {
  return {
    tokenGetter: () => {
      return storage.get('access_token');
    }
  };
}

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    JwtModule.forRoot({
      jwtOptionsProvider: {
        provide: JWT_OPTIONS,
        useFactory: jwtOptionsFactory,
        deps: [LocalStorageService]
      }
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClientTranslator]
      }
    })
  ],
  providers: [
    StatusBar,
    SplashScreen,
    ApiService,
    LaunchNavigator,
    AndroidPermissions,
    Geolocation,
    GoogleMaps,
    LocationAccuracy,
    Health,
    LocalStorageService,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: LOCALE_ID, useValue: 'es' },
    { provide: HTTP_INTERCEPTORS, useClass: AuthenticatorInterceptor, multi: true },
    AppVersion,
    Market,
    FirebaseX, 
    InAppBrowser
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(translate: TranslateService) {
    translate.setDefaultLang('es');
  }
}
